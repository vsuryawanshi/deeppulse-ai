import React, {Component} from "react";

export default class WeatherTags extends Component{
    render(){
        var imageToShow = null;
        if(this.props.tagData[0].tag[0] && this.props.tagData[0].tag[0].value[0]){
            var responseWeather = this.props.tagData[0].tag[0].value[0];
            if(responseWeather.toLowerCase() == "sunny"){
                imageToShow = require("../../images/sunny.svg");
                if(this.props.tagData[0].tag[0].value[1]){
                    imageToShow = require("../../images/sunandcloud.svg");
                }
            } else if(responseWeather.toLowerCase() == "cloudy"){
                imageToShow = require("../../images/clouds.svg");
            } else if(responseWeather.toLowerCase() == "snowy"){
                imageToShow = require("../../images/snowy.svg");
            }
        }
        return(
            <div className="weather card">
                <div className="card-inner">
                    <div className="chead">{this.props.tagData[0].displayName}</div>
                    <div className="weather-content">
                        {
                            this.props.tagData[0].tag[0] && this.props.tagData[0].tag[0].value.length > 0 ?
                            <div className="weather-type">
                                {
                                    imageToShow !== null ?
                                    <div className="wimg">
                                        <img src={imageToShow}/>
                                    </div>
                                    :
                                    null
                                }
                                {this.props.tagData[0].tag[0].value.join(", ")}                            
                            </div>
                            :
                            <div className="generic-no">Could not determine</div>
                        }
                    </div>
                </div>
            </div>
        )
    }
}