import React, { Component } from 'react';
import ReactDOM from "react-dom";
import {FIRST_ANIM, SECOND_ANIM, THIRD_ANIM, FOURTH_ANIM} from "../common/constants";
import $ from "jquery";
import Footer from "../common/FooterComp";

export default class MinaTechComp extends Component {
    constructor(props){
        super(props);
        this.state = {
            chips :[],
            animateText:"Our global location intelligence engine understands the DNA of every location on earth",
            animateText1:"And is used in sending personalized messages to every customer based on their preferences and needs.",
            currentTitle:"DNAfication of global information"
        };
    }

    windowLoadedCompletely(){
		var loaderElem = document.getElementById("loader-wrap");
		loaderElem.style.display = "none";
		setTimeout(()=>{
			//this.startImageAnimation();
		},200);
    }
    
    componentDidMount() {
        this.windowLoadedCompletely();
        this.moveToFourthImage();
    }

    extractAndShowChips(arr, callback){
        var loopIndex = 1;
        this.cinterval = window.setInterval(()=>{
            this.setState({chips:[]});
            var slicedArray = arr.slice(0,loopIndex++);
            this.setState({chips:slicedArray});
            if(loopIndex == arr.length + 1){
                clearInterval(this.cinterval);
                setTimeout(()=>{
                    callback();
                },2000);
            }
        },1000);   
    }

    moveToFourthImage(){
        var a4 = ReactDOM.findDOMNode(this.refs.anim4);
        a4.style.display = "block"; 
        this.ct = setTimeout(()=>{ 
            ReactDOM.findDOMNode(this.refs.zoomlens3).classList.add("path2");
            var topPosition = $(a4).position().top + $(a4).height() + 70;
            $(".chips-wrap").css({top:topPosition+"px"});
            this.extractAndShowChips(FOURTH_ANIM, ()=>{
                this.resetSecondPageAnimations();
                this.moveToFourthImage();
            });
        },1000);
    }

    resetSecondPageAnimations(){
        ReactDOM.findDOMNode(this.refs.zoomlens3).classList.remove("path2");
        this.setState({chips:[]});
        window.clearTimeout(this.ct);
        window.clearInterval(this.cinterval);
    }

    componentWillUnmount(){
        window.clearTimeout(this.ct);
        window.clearInterval(this.cinterval);
    }
    
    render() {
        return (
            <div className="technology-wrap">
                <div className="pattern-bg"/>
                <div className="info-section">
                    <div className="container">
                        <div className="texte">
                            {this.state.animateText}
                            <div className="ts">
                                {this.state.animateText1}
                            </div>
                            <div className="cur-title">{this.state.currentTitle}</div>
                        </div>
                        <div className="post-card first right" ref="anim4" style={{"display":"none"}}>
                            <div className="zoomlens" ref="zoomlens3"/>
                            <div ref="bgimage" className="img-holder" style={{"backgroundImage":"url(http://www.thehindu.com/news/cities/Delhi/article20004885.ece/alternates/FREE_660/DELHISMOGPOLLUTION)"}}/>
                        </div>
                        <div className="chips-wrap">
                        {
                            this.state.chips.map((chipItem, index)=>{
                                return(
                                    <div className="chip" key={index}>
                                        <div className="ctext">{chipItem.text}</div>
                                        <div className="cicon">
                                            <img src={chipItem.image}/>
                                        </div>
                                        <div className="cpercent">{chipItem.percent}</div>
                                    </div>
                                );
                            })
                        }
                        </div>
                    </div>
                </div>
                <Footer />
            </div>
        );
    }
}