import React, {PureComponent} from 'react';
export default class AsyncComponent extends PureComponent {
    constructor(props) {
        super(props);

        this.state = {
            Component: null
        }
    }

    componentWillMount() {
        if (!this.state.Component) {
            this.props.moduleProvider().then(({Component}) => this.setState({Component}));
        }
    }

    render() {
        const {Component} = this.state;

        //The magic happens here!
        console.log(Component);
        return (
            <div>
                {Component ? <Component /> : <div id="loader-wrap">
        <div className="loader"></div>
    </div>}
            </div>
        );
    }
};