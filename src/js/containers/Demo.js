import React, { Component } from 'react';
import axios from "axios";
import Img from 'react-image'
import {findDOMNode} from "react-dom";
import FaceTag from "../partials/FaceTag";
import NSFWTag from "../partials/NSFWTag";
import QualityTag from "../partials/QualityTag";
import SceneAttributes from "../partials/SceneAttributes";
import SceneCategories from "../partials/SceneCategories";
import SceneEnvironment from "../partials/SceneEnvironment";
import SemanticTags from "../partials/SemanticTags";
import WeatherTags from "../partials/WeatherTags";
import Landmark from "../partials/Landmark";


const DEMO_DATA = [
	"http://62.210.100.210:8099/skyscraper.jpg",
	"http://62.210.100.210:8099/bathroom.jpg",
	"https://i.pinimg.com/originals/e5/13/1b/e5131b97afcf24beb6856b7db9c54672.jpg",
	"http://62.210.100.210:8099/golden.jpg",
	"http://www.elitetraveler.com/wp-content/uploads/2013/08/Atlantis-The-Palm-4.jpg",
	"http://62.210.100.210:8099/qutub2.jpg",
	"https://i.imgur.com/Up4a1F3.jpg",
	"https://luxuryescapes.com/magazine/wp-content/uploads/2016/09/unique-ice-hotel-in-sweden-4-Copy.jpg",
	"http://62.210.100.210:8099/crowd.jpg",
	"http://62.210.100.210:8099/person.jpg",
	"http://62.210.100.210:8099/food.jpg",
];
export default class DemoPage extends Component {
	constructor(props){
		super(props);

		this.state = {
			currentImageUrl:"",
			currentImageSource:"",
			imageLoaded:false,
			callingImageApi:false,
			imgMetaData:{},
			selectedImageIndex:0,
			canvasHeight:300,
			canvasWidth:300,
			expandUploadArea:false,
		}

		window.onload = () => {
            this.windowLoadedCompletely();
		};
		
		this.originalWidth = 0;
		this.originalHeight = 0;
	}


	windowLoadedCompletely(){
        var loaderElem = document.getElementById("loader-wrap");
        loaderElem.style.display = "none";
	}

	isURL(str) {
        var pattern = new RegExp('^(https?:\\/\\/)?'+ // protocol
        '((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|'+ // domain name and extension
        '((\\d{1,3}\\.){3}\\d{1,3}))'+ // OR ip (v4) address
        '(\\:\\d+)?'+ // port
        '(\\/[-a-z\\d%@_.~+&:]*)*'+ // path
        '(\\?[;&a-z\\d%@_.,~+&:=-]*)?'+ // query string
        '(\\#[-a-z\\d_]*)?$','i'); // fragment locator
        return pattern.test(str);
    }
	
	handleImageSubmit(){
		this.clearImageCanvas();
		this.setState({
			expandUploadArea:false,
			currentImageSource:this.state.currentImageUrl,
			selectedImageIndex:-1,
		},()=>{
			this.getImageData(this.state.currentImageUrl);
		})
	};

	getImageData(imgUrl){
		this.setState({loading:true});
		//first cancel any api request
		//source.cancel("Operation cancelled by user");
		axios.get("http://62.210.100.210:8080/images/insight?url=" + encodeURI(imgUrl)).then((response)=>{
			var targetObject = {};
			Object.keys(response.data.data).map((ckey)=>{
				if(ckey != "DP_PLC"){
					targetObject[ckey] = response.data.data[ckey];
				} else {
					response.data.data[ckey].map((pl)=>{
						if(pl.displayName == "Scene Categories"){
							targetObject["DP_PLC_SCN_CT"] = pl;
						} else if(pl.displayName == "Scene Attributes"){
							targetObject["DP_PLC_SCN_SA"] = pl;
						} else if(pl.displayName == "Scene Environment"){
							targetObject["DP_PLC_SCN_SE"] = pl;
						}
					});
				}
				if(ckey.toUpperCase() == "DP_FCE"){
					this.drawTimeout = setTimeout(()=>{
						this.drawFaceBoundaries(response.data.data[ckey][0].attrs[0].shape,response.data.data[ckey][0].attrs[1].gender);
					},500);
				}
				if(ckey.toUpperCase() == "DP_QUA"){
					var quality = response.data.data[ckey];
					quality.map((qtag)=>{
						if(qtag.DP_NAME.toUpperCase() == "DP_QUA_RES"){
							qtag.tag.map((qqt)=>{
								if(qqt.tag_name.toUpperCase() == "DP_QUA_BRT_WD"){
									this.originalWidth = qqt.value;
								}
								if(qqt.tag_name.toUpperCase() == "DP_QUA_BRT_HT"){
									this.originalHeight = qqt.value;
								}
							});
						}
					});
				}
			});

			this.setState({
				loading : false,
				imgMetaData : targetObject,
				loading:false,
				currentImageUrl:""
			});
		}).catch((thrown)=>{
			console.log("error in getting image info");
			this.setState({
				loading : false,
				imgMetaData : {},
				loading:false
			});
		})
	}

	componentDidMount(){
		clearInterval(global.animationInterval);
		clearTimeout(global.currentAnimeTimeout);
		setTimeout(()=>{
			this.setState({
				currentImageSource:DEMO_DATA[0],
				selectedImageIndex:0,
				loading:true
			},()=>{
				this.getImageData(DEMO_DATA[0]);
			})
		},500);
	}

	showNextCard(){
		var arr = document.getElementsByClassName("card");
		var topmostItem = arr[0];
		for(var i = 1; i < arr.length; i++){
			if(arr[i].style.zIndex > topmostItem.style.zIndex){
				topmostItem = arr[i];
			}
		}
		topmostItem.classList.add("go-away");
		setTimeout(()=>{
			var first = arr[0].style.zIndex;
			for(var i=0;i<arr.length;i++){
				if(i<arr.length-1){
					arr[i].style.zIndex = arr[i+1].style.zIndex;
				} else {
					arr[i].style.zIndex = first;
				}
			}
			setTimeout(()=>{
				topmostItem.classList.remove("go-away");
			},300);
		},400);
	}

	handleImageChange(index){
		this.clearImageCanvas();
		this.setState({
			currentImageSource:DEMO_DATA[index],
			selectedImageIndex:index,
			loading:true
		},()=>{
			this.getImageData(DEMO_DATA[index])
		});
	};

	clearImageCanvas(){
		var faceCanvas = findDOMNode(this.refs.faceCanvas);
		var ctx = faceCanvas.getContext('2d');
		ctx.clearRect(0, 0, faceCanvas.width, faceCanvas.height);
	}

	drawFaceBoundaries(boundaryData,genderData){
		if(boundaryData.length > 0 && (this.originalWidth != 0 || this.originalHeight != 0)){
			var currentImage = findDOMNode(this.refs.originalImage);
			var faceCanvas = findDOMNode(this.refs.faceCanvas);

			this.setState({
				canvasWidth:currentImage.clientWidth,
				canvasHeight:currentImage.clientHeight
			});
			var widthFactor = parseFloat(currentImage.clientWidth / this.originalWidth);
			var heightFactor = parseFloat(currentImage.clientHeight / this.originalHeight);
			var ctx = faceCanvas.getContext('2d');
			
			ctx.font = "12px Helvetica";
			ctx.fillStyle = "red";
			boundaryData.map((rect,index)=>{
				var rectX = rect[0] * widthFactor;
				var rectY = rect[1] * heightFactor;
				var width = (rect[2] - rect[0]) * widthFactor;
				var height = (rect[3] - rect[1]) * heightFactor;
				ctx.beginPath();
				ctx.lineWidth="2";
				if(genderData[index] && genderData[index][0] && genderData[index][0] == "woman"){
					ctx.strokeStyle="#f90e8c";
				} else {
					ctx.strokeStyle="#00B2FF";
				}
				ctx.rect(rectX, rectY, width, height); 
				ctx.stroke();
				if(genderData.length > 0){
					var textToShow = "";
					if(genderData[index][0] != "" && genderData[index][1])
					textToShow = genderData[index][0] + " - " + parseFloat(genderData[index][1] *100).toFixed(1) + "%";
					ctx.fillText(textToShow,rectX,rectY-10);
				}
			});
		}
	}

	render() {
		const settings = {
			dots: false,
			infinite: true,
			speed: 500,
			slidesToShow: 1,
			slidesToScroll: 1
		};
		return (
			<div className="demo-container">
				<div className="demo-wrapper">	
					<div className="media-explorer">
						<div className="demo-explorer-container">
							<div className={`media-view` + (this.state.loading ? " fade" : "")}>
								<div className="image-view">
									<Img 
										src={this.state.currentImageSource} 
										loader={<img src={require("../../images/imgloader.gif")} style={{"width":"100px"}}/>}
										ref="originalImage"/>
									
									<canvas ref="faceCanvas" className="face-canvas" width={this.state.canvasWidth} height={this.state.canvasHeight}/>
								</div>
							</div>
							<div className={`media-list-container` + (this.state.loading ? " fade" : "")}>
								<ul className="demo-explorer-media-list">
									{
										DEMO_DATA.map((demoImage,index)=>{
											return(
												<li 
													key={index} 
													className={`demo-explorer-media-item` + (this.state.selectedImageIndex == index ? " selected" : "")} 
													style={{"backgroundImage": "url(" +demoImage +")"}}
													onClick={()=>{
														if(!this.state.loading){
															this.handleImageChange(index);
														}
													}}/>
											);
										})
									}
								</ul>
							</div>
						</div>
					</div>					
					<div className="model-content demo-model-info no-scroll">
						<div className="demo-info-header">
							<div className="demo-info-title">
								{
									this.state.loading ?
									"Processing image, please wait"
									:
									"Extracted Image data"
								}
							</div>
						</div>
						{
							this.state.loading ? 
							<div className="loader-wrap">
								<img src={require("../../images/loader.gif")}/>
							</div>
							:
							<div className={`results` + (Object.keys(this.state.imgMetaData).length > 0 ? " show" : "")}>
								{
									Object.keys(this.state.imgMetaData).map((currentKey,index)=>{
										var zidToGive = index + 1;
										switch(currentKey){
											case "DP_FCE":
												return(
													<FaceTag tagData={this.state.imgMetaData[currentKey]} key={index}/>
												);
											case "DP_NSF":
												return(
													<NSFWTag tagData={this.state.imgMetaData[currentKey]} key={index}/>
												);
											case "DP_QUA":
												return(
													<QualityTag	tagData={this.state.imgMetaData[currentKey]} key={index}/>
												);
											case "DP_PLC_SCN_CT":
												return(
													<SceneCategories tagData={this.state.imgMetaData[currentKey]} key={index}/>
												);
											case "DP_PLC_SCN_SA":
												return(
													<SceneAttributes tagData={this.state.imgMetaData[currentKey]} key={index}/>
												);
											case "DP_PLC_SCN_SE":
												return(
													<SceneEnvironment tagData={this.state.imgMetaData[currentKey]} key={index}/>
												);
											case "DP_SMN":
												return(
													<SemanticTags tagData={this.state.imgMetaData[currentKey]} key={index}/>
												);
											case "DP_WEA":
												return(
													<WeatherTags tagData={this.state.imgMetaData[currentKey]} key={index}/>
												);
											case "DP_LNM":
												return(
													<Landmark tagData={this.state.imgMetaData[currentKey]} key={index}/>
												);
											default:
												return null;
										}	
									})
								}
							</div>
						}
						<div className="show-upload">
							<div className="top" onClick={(event)=>{
								this.setState({
									expandUploadArea : !this.state.expandUploadArea
								});
							}}>
								<svg xmlns="http://www.w3.org/2000/svg" width="26" height="26" viewBox="0 0 26 26">
									<g fill="none" fillRule="evenodd">
										<path stroke="#FFF" strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M13 5H1v20h20V13M21 1v8M25 5h-8"></path>
										<path stroke="#FFF" strokeLinejoin="round" strokeWidth="2" d="M15 14l-6 6-3-3-5 5v3h20v-5z"></path>
										<path fill="#FFF" d="M8 10a2 2 0 1 1-4.001-.001A2 2 0 0 1 8 10"></path>
									</g>
								</svg>Try your own image
							</div>
							<div className={`bottom` + (this.state.expandUploadArea ? " show" : "")}>
								<input 
									type="text" 
									placeholder="Enter your image URL"
									value={this.state.currentImageUrl}
									onChange={(event)=>{
										this.setState({currentImageUrl : event.target.value})
									}}/>
									{
										!this.isURL(this.state.currentImageUrl) && this.state.currentImageUrl.length > 0 ?
										<div className="err">Please enter a valid url</div>
										:
										null
									}
								<div className="btn-wrap">
									<button className="btn primary" disabled={!this.isURL(this.state.currentImageUrl) || this.state.loading} onClick={(event)=>{
										this.handleImageSubmit();
									}}>Go</button>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		);
	}
}