import React, { Component } from 'react';

export default class FaceTag extends Component {
    render() {
        return (
            <div className={`ftw card` + (!this.props.tagData[0] ? " hide" : "")}>
            {
                this.props.tagData[0] ?
                <div className="card-inner">
                    <div className="chead">{this.props.tagData[0].displayName}</div>
                    {
                        this.props.tagData[0].tag[0].value > 0 ?
                            <div className="color-bg">
                                <div className="faces-val">{this.props.tagData[0].tag[0].value}<div className="faces-lbl">
                                    {
                                        this.props.tagData[0].tag[0].value == 1 ? "face ": "faces "
                                    }
                                detected</div></div>
                            </div>
                        :
                        <div className="generic-no">No face detected</div>
                    }
                </div>
                :
                null
            }
            </div>
        );
    }
}