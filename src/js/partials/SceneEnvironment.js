import React, { Component } from 'react';

export default class SceneEnvironment extends Component {
    render() {
        return (
            <div className="places card">
                <div className="card-inner">
                    <div className="chead">{this.props.tagData.displayName}</div>
                    {
                        this.props.tagData.tag[0] && this.props.tagData.tag[0].value.length > 0 ?
                        <div className="places-content">
                            {
                                this.props.tagData.tag[0].value.map((attr,index)=>{
                                    return(
                                        <div className="place-env" key={index}>{attr}</div>
                                    );
                                })   
                            }
                        </div>
                        :
                        <div className="generic-no">
                            No Scene Environment identified
                        </div>
                    }
                </div>
            </div>
        );
    }
}