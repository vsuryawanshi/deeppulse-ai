import React, {Component} from "react";
import Footer from "../common/FooterComp";

export default class PrivacyPage extends Component{
    windowLoadedCompletely(){
		var loaderElem = document.getElementById("loader-wrap");
		loaderElem.style.display = "none";
		setTimeout(()=>{
			//this.startImageAnimation();
		},200);
	}
	componentDidMount() {
		//this.loadImageOnCanvas(require("../../images/3.jpg"));       
		this.windowLoadedCompletely();
	}
    render(){
        return(
            <div className="privacy-wrap">
                <div className="container">
    	<section className="heading-text">
            <div className="title"><h1>Privacy Policy</h1></div>
        </section>
            
            
			            
            <h2>Declaration on data protection</h2>
<p>DeepPulse Labs Private Limited appreciates your visit to our Web site and thanks you for your interest in our company, our products and our services. Protection of your privacy and personal data is an important matter for us. In order to guarantee you a high level of transparency and security, our declaration on data protection as set forth below will inform you as to the nature, scope and purpose of any collection, use and processing of information and data recorded by us.</p>
<p>In this respect, please keep in mind that this declaration on data protection refers only to the publicly accessible Web sites with the following Internet addresses/URLs of DeepPulse Labs Private Limited. You may read them here.</p>
<p><strong>1. External Links</strong><br/>
Our Web site also makes reference to third parties. As a rule, these are identified by stating the relevant Internet address or the company/product logo. DeepPulse Labs Private Limited has no influence whatsoever on the contents and design of sites of other providers and therefore cannot extend the warranties under this declaration on data protection to such sites. Nor does the fact that we refer to such sites mean that we adopt their contents as our own.</p>
<p><strong>2. Cookies</strong><br/>
Our Web sites use so-called session cookies. These cookies deposit data for purposes of technical regulation in your browser’s memory. However, they do not contain any personal data and are deleted after your browser is closed.</p>
<p>We do deposit cookies on your computer, which are not deleted after conclusion of the session. These make it possible to recognize your computer when the forums are next visited. Such cookies make it possible for us to adapt the Web site to your interests. For U.S.-based visitors, we may also use cookies to present banners to you across the Internet based</p>
<p>on your interest. With your consent, we store your password so that you do not have to re-enter it each time.</p>
<p>Moreover, it is possible for you to set your browser such that cookies are rejected or you are informed prior to storage thereof.</p>
<p>Flash-Cookies or Local Shared Objects (LSO) will not be used by DeepPulse Labs Private Limited. Also Session Storage or DOM Storage will not be used.</p>
<p><strong>3. Communication-related and Use-related Data</strong><br/>
Generally speaking, you may visit DeepPulse Labs Private Limited’s Web site without us finding out who you are. Only for statistical and internal system-related purposes do we record the access time, the quantity of data transferred, the pages of ours that you visited, the page from which you accessed our Web site and the browser that you use (log files), in addition to you IP address. To the extent that such information might enable conclusions in relation to personal data, these are naturally subject to the relevant statutory data protection provisions. The data is not personalized. The data is deleted following statistical evaluation.</p>
<p><strong>4. Collection of Personal Data</strong><br/>
In order to be able to utilize the offers made on our Web site, in particular, downloads, online forums, online applications, individual enquiries, registrations for our courses or registrations for other services, it is necessary that we obtain personal data from you. The personal data entails individual statements concerning personal and material relations pertaining to you, such as your name, your address or your e-mail address. Such information is collected by us only if you provide it voluntarily and show us in this way that you agree to the use and processing thereof. In this respect, it goes without saying for us that such data is necessarily only collected where it is absolutely mandatory for implementation of the relevant offers. Where we request additional data, this occurs solely for purposes of, for example, facilitating communication with you or improving our service.</p>
<p><strong>5. Use and Processing of Personal Data</strong><br/>
As a rule, DeepPulse Labs Private Limited uses and processes the personal data provided for purposes of contractual settlement and processing your enquiries and requests.</p>
<p>Data with personal content is processed and used to a limited extent for customer management and marketing purposes, provided the relevant statutory provisions permit this. Should you no longer agree to such use, you may, of course, object to further use at any time.</p>
<p>Moreover, your information provided to us may be combined with data on you that has already been stored by us. Such data combination has, inter alia, the advantage of keeping your customer data collected by us updated at all times, guaranteeing the accuracy of the collected information and being able to optimize our service to you.</p>
<p>We use and process your data outside these parameters only if you have expressly granted your prior consent thereto and only for purposes of which you have been advised. Thus, for example, you may stipulate in an online application whether we may review the application with respect to other positions in our company. Only in this event will we use such data for this purpose.</p>
<p><strong>6. Forwarding of Data</strong><br/>
DeepPulse Labs Private Limited is a group that does business globally. To this extent, in order to better process your matter, it might be necessary to forward your data to local subsidiaries or to local distribution partners, whose registered office might also be located in states outside the EU. However, such transmission takes place only within the group and only for the aforementioned purposes.</p>
<p>Naturally we do not forward your data to other third parties, unless we are obliged to do so by virtue of statutory provisions or judicial order.</p>
<p><strong>7. E-mail Contact</strong><br/>
Should you provide us with your e-mail address, we will also communicate with you by e-mail. Should you no longer desire such information, it is naturally possible for you at any time to revoke your consent. In this case, please send us a short message to the given postal or e-mail address.</p>
<p><strong>8. Participation in Forums</strong><br/>
On our “Developer Community” Web site, we offer you the possibility of participating in online forums. To this end, we require sufficient information in order to be able to clearly identify you so that, where applicable, we can meet our duty to subsequently identify authors of illegal content. Your personal data is not disclosed to other forum participants unless you have consented thereto in your user profile. Please keep in mind that the texts published by you may be viewed in part by everyone without limitation on the Internet. To this extent, please observe our Terms of Use when registering for our online forums. Technical administration and hosting of the forums is conducted on our behalf and is controlled by us, in part, outside the EU as well.</p>
<p><strong>9. Data Protection for Minors</strong><br/>
The offers on this Web site are not directed at children. We therefore assume that we will not become aware of any personal data pertaining to children.</p>
<p><strong>10. Technical and Organizational Data Protection</strong><br/>
Naturally, DeepPulse Labs Private Limited takes those technical and organizational measures appropriate given the respective purpose of the data protection in order to protect the information provided by you against abuse and loss. Such data is stored in a secure operating environment that is not accessible to the public. In addition, each of our employees is naturally instructed on data protection and obliged to enter into a confidentiality covenant.</p>
<p><strong>11. E-Mails to DeepPulse Labs Private Limited</strong><br/>
Because of tax requirements DeepPulse Labs Private Limited has to archive e-mails for ten years in some countries. The e-mail system of DeepPulse Labs Private Limited and other members of the DeepPulse Labs Private Limited Group are intended to be used only for business purposes.</p>
<p><strong>12. Modification of the Declaration on Data Protection</strong><br/>
As a result of the ongoing development of the Internet, it will be necessary to adapt this declaration on data protection to changing conditions on an ongoing basis. Timely notice of such modification shall be given on this page. You may view previous versions of this declaration on data protection in this Archive.</p>
<p><strong>13. Procedural List</strong><br/>
You may obtain a general overview of the purpose for which and the group of persons on which DeepPulse Labs Private Limited collects, processes and uses personal data in the public Procedural List shown on this Web site.</p>
<p><strong>14. Information and Other Rights, Contact</strong><br/>
Upon written request, our data protection officer would be happy to provide you with information as to whether – and if so, which – personal data we store in relation to you. Should your personal data be incorrect, you may have this rectified immediately. Naturally any such information or modification is free of charge.</p>
<p>Moreover, you are entitled to revoke your consent to use of data in the future, in whole or in part. Should you desire this, we will delete or block your relevant data.</p>
<p>In order to assert such rights, please contact DeepPulse Labs Private Limited’s data protection officer.</p>
<p>Naturally, you may contact DeepPulse Labs Private Limited’s data protection officer directly at any time should you have questions, comments or complaints in connection with this declaration on data protection:</p>
<p><strong>DeepPulse Labs Private Limited</strong><br/>
Phase 11,<br/>
Mohali,<br/>
Punjab,<br/>
INDIAN,<br/></p>
            
                            
        
	            </div>
                <Footer />
            </div>
        );
    }
}