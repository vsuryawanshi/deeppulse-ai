import React, { Component } from 'react';
import ReactSvgPieChart from "react-svg-piechart";

export default class SemanticTags extends Component {
    constructor(props){
        super(props);
        this.pieChartData = [];
        if(this.props.tagData[0] && this.props.tagData[0].tag[0]){
            this.props.tagData[0].tag[0].value.map((attrType,index)=>{
                var tempObj = {};
                tempObj["title"] = attrType.tag_name;
                tempObj["value"] = attrType.value;
                tempObj["color"] = this.getRandomColor();
                this.pieChartData.push(tempObj);
            });
        }
    }
    getRandomColor() {
        var letters = '0123456789ABCDEF';
        var color = '#';
        for (var i = 0; i < 6; i++) {
          color += letters[Math.floor(Math.random() * 16)];
        }
        return color;
    }

    render() {
        return (
            <div className={`semantic card` + (!this.props.tagData[0] ? " hide" : "")}>
            {
                this.props.tagData[0] ?
                <div className="card-inner">
                    <div className="chead">{this.props.tagData[0].displayName}</div>
                    {
                        this.pieChartData.length > 0 ?
                        <div className="semantic-content">
                            <div className="pie-container">
                                <ReactSvgPieChart data={this.pieChartData} expandOnHover={false}/>
                            </div>
                            <div className="data-list">
                                {
                                    this.pieChartData.map((semantic,index)=>{
                                        return(
                                            <div className="semantic-item" key={index}>
                                                <div className="color-box" style={{"backgroundColor":semantic.color}}/>
                                                <div className="semantic-name">{semantic.title} ({parseFloat(semantic.value).toFixed(3)}%)</div>
                                            </div>
                                        );
                                    })
                                }
                            </div>
                            {
                                this.props.tagData[0].tag[1] && this.props.tagData[0].tag[1].value ?
                                <div className="zoom-info">Image is zoomed</div>
                                :
                                null
                            }
                        </div>
                        :
                        <div className="generic-no">
                            No Semantics captured in the image
                        </div>
                    }
                </div>
                :
                null
            }
            </div>
        );
    }
}