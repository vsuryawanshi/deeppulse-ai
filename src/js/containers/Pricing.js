import React, {Component} from "react";
import Footer from "../common/FooterComp";

export default class PricingPage extends Component{
    windowLoadedCompletely(){
		var loaderElem = document.getElementById("loader-wrap");
		loaderElem.style.display = "none";
		setTimeout(()=>{
			//this.startImageAnimation();
		},200);
	}
	componentDidMount() {
		//this.loadImageOnCanvas(require("../../images/3.jpg"));       
		this.windowLoadedCompletely();
	}
    render(){
        return(
            <div className="pricing-wrap">
                <section className="price-list">
					<h2>Our pricing plans</h2>
					<div className="plans-wrap">
						<div className="plan">
							<div className="head">
								PLAN A
							</div>
							<div className="plan-details">
								This plan will consist of features A,B,C. This plan will consist of features A,B,C. This plan will consist of features A,B,C. This plan will consist of features A,B,C. This plan will consist of features A,B,C. 
							</div>
							<div className="btn-wrap">
								<div className="details-btn">See Details</div>
								<div className="buy-btn">Buy Now</div>
							</div>
						</div>
						<div className="plan">
							<div className="head">
								PLAN B
							</div>
							<div className="plan-details">
								This plan will consist of features A,B,C. This plan will consist of features A,B,C. This plan will consist of features A,B,C. This plan will consist of features A,B,C. This plan will consist of features A,B,C. 
							</div>
							<div className="btn-wrap">
								<div className="details-btn">See Details</div>
								<div className="buy-btn">Buy Now</div>
							</div>
						</div>
						<div className="plan">
							<div className="head">
								PLAN C
							</div>
							<div className="plan-details">
								This plan will consist of features A,B,C. This plan will consist of features A,B,C. This plan will consist of features A,B,C. This plan will consist of features A,B,C. This plan will consist of features A,B,C. 
							</div>
							<div className="btn-wrap">
								<div className="details-btn">See Details</div>
								<div className="buy-btn">Buy Now</div>
							</div>
						</div>
						<div className="plan">
							<div className="head">
								PLAN D
							</div>
							<div className="plan-details">
								This plan will consist of features A,B,C. This plan will consist of features A,B,C. This plan will consist of features A,B,C. This plan will consist of features A,B,C. This plan will consist of features A,B,C. 
							</div>
							<div className="btn-wrap">
								<div className="details-btn">See Details</div>
								<div className="buy-btn">Buy Now</div>
							</div>
						</div>
					</div>
				</section>
				<Footer />
            </div>
        );
    }
}