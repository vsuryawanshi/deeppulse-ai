import React, {Component} from "react";

export default class Landmark extends Component{
    render(){
        return(
            <div className={`landmark card`}>
                <div className="card-inner">
                    <div className="chead">{this.props.tagData[0].displayName}</div>
                    {
                        this.props.tagData[0].tag[0] && this.props.tagData[0].tag[0].value != "" ?
                        <div className="landmark-content">
                            {this.props.tagData[0].tag[0].value[0]}
                        </div>
                        :
                        <div className="generic-no">
                            No Landmarks identified
                        </div>
                    }
                </div>
            </div>
        )
    }
}