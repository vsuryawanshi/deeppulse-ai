import React, { Component } from 'react';
import { Router, browserHistory } from 'react-router';
import ReactDOM from "react-dom";
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import TextAnimate from "../common/TextAnimate";
import routes from '../routes';
import nav from "../nav";

export default class AppContainer extends Component {
    constructor(props){
        super(props);
        this.state = {
            showMobileMenu:false,
            selectedNav:-1
        }
    }

    render() {
        return (
            <MuiThemeProvider>
                <div className="main-wrap">
                    <header className="landing-header">
                        <div className="header-inner">
                            <div className="main-title" onClick={()=>{
                                this.setState({
                                    selectedNav:-1
                                })
                                browserHistory.push("/");   
                            }}>
                                <img src={require("../../images/deeppulse.svg")}/>
                                <div className="tagl">
                                    <TextAnimate items={["Think like humans","Work like machines"]} timeout={3000} ref="taHead"/>
                                </div>
                            </div>
                            <label id="hamburger" className="hamburger icon icon-hamburger" onClick={(event)=>{
                                this.setState({showMobileMenu : true});
                            }}></label>
                            <nav className={`header-nav` + (this.state.showMobileMenu ? " show" : "")}>
                                <nav className="navbar-list">
                                    {
                                        nav.items.map((navItem,index)=>{
                                            if(navItem.submenu){
                                                return(
                                                    <div key={index} className={`nav-dropdown` + (navItem.name == "Preview" ? " hidemobile" : "")} ref={"menu" + index}>
                                                        <div className="dropdown-trigger">{navItem.name}</div>
                                                        <div className="dropdown-content">
                                                            {
                                                                navItem.submenu.map((smenu,sindex)=>{
                                                                    return(
                                                                        <div 
                                                                            className={`dropdown-item` + (smenu.url == "" ? " disabled" : "") + (smenu.type ? " indent" : "")} 
                                                                            key={sindex} 
                                                                            onClick={()=>{
                                                                                this.setState({
                                                                                    showMobileMenu:false,
                                                                                    selectedNav:index
                                                                                },()=>{
                                                                                    if(smenu.url !== ""){
                                                                                        ReactDOM.findDOMNode(this.refs["menu"+index]).classList.add("open");
                                                                                        setTimeout(()=>{
                                                                                            ReactDOM.findDOMNode(this.refs["menu"+index]).classList.remove("open");
                                                                                        },500);
                                                                                        browserHistory.push(smenu.url);
                                                                                        document.body.scrollTop = 0; // For Safari
                                                                                        document.documentElement.scrollTop = 0; // For Chrome, Firefox, IE and Opera
                                                                                    }
                                                                                })
                                                                        }}>
                                                                        {smenu.name}
                                                                        </div>
                                                                    );
                                                                })
                                                            }
                                                        </div>
                                                    </div>
                                                );
                                            } else {
                                                return(
                                                    <div 
                                                        key={index}
                                                        className={`nav-item` + (this.state.selectedNav === index ? " active":"")} 
                                                        onClick={(event)=>{
                                                            this.setState({
                                                                showMobileMenu:false,
                                                                selectedNav:index
                                                            },()=>{
                                                                browserHistory.push(navItem.url);
                                                                document.body.scrollTop = 0; // For Safari
                                                                document.documentElement.scrollTop = 0; // For Chrome, Firefox, IE and Opera
                                                            })
                                                    }}>{navItem.name}</div>
                                                );
                                            }
                                        })
                                    }
                                </nav>
                                <nav className="nav-auth">
                                    <div className="nav-item nav-btn" onClick={()=>{
                                        this.setState({
                                            showMobileMenu:false
                                        },()=>{
                                            browserHistory.push("contact");
                                            document.body.scrollTop = 0; // For Safari
                                            document.documentElement.scrollTop = 0; // For Chrome, Firefox, IE and Opera
                                        })
                                    }}>
                                        <a>Contact Sales</a>
                                    </div>
                                    <div className="close-icon" onClick={(event)=>{
                                        this.setState({showMobileMenu:false})
                                    }}/>
                                </nav>
                            </nav>
                        </div>
                    </header>
                    <Router routes={routes} history={browserHistory}/>
                </div>
            </MuiThemeProvider>
        );
    }
}