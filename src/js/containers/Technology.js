import React, { Component } from 'react';
import ReactDOM from "react-dom";
import {FIRST_ANIM, SECOND_ANIM, THIRD_ANIM, FOURTH_ANIM} from "../common/constants";
import $ from "jquery";
import Footer from "../common/FooterComp";

export default class MinaTechComp extends Component {
    constructor(props){
        super(props);
        this.state = {
            chips :[],
            animateText:"DeepPulse's cutting-edge computer vision capabilities",
            animateText1:"analyzes hotel images through hundreds of different parameters and promising to present the right picture in the right context.",
            currentTitle:"Right information in right context"
        };
    }

    windowLoadedCompletely(){
		var loaderElem = document.getElementById("loader-wrap");
		loaderElem.style.display = "none";
		setTimeout(()=>{
			//this.startImageAnimation();
		},200);
    }
    
    componentDidMount() {
        this.windowLoadedCompletely();
        this.startSecondPageAnimations();
    }

    extractAndShowChips(arr, callback){
        var loopIndex = 1;
        this.cinterval = window.setInterval(()=>{
            this.setState({chips:[]});
            var slicedArray = arr.slice(0,loopIndex++);
            this.setState({chips:slicedArray});
            if(loopIndex == arr.length + 1){
                clearInterval(this.cinterval);
                setTimeout(()=>{
                    callback();
                },2000);
            }
        },1000);   
    }

    startSecondPageAnimations(){
        var elem = ReactDOM.findDOMNode(this.refs.anim1);
        elem.style.display = "block";
        this.setState({
            animateText:"DeepPulse's cutting-edge computer vision capabilities",
            animateText1:"Analyzes images through hundreds of different parameters, promising to present the right picture in the right context.",
        })
        this.ct = setTimeout(()=>{
            ReactDOM.findDOMNode(this.refs.zoomlens).classList.add("path1");
            var topPosition = $(elem).position().top + $(elem).height() + 70;
            $(".chips-wrap").css({top:topPosition+"px"});
            this.extractAndShowChips(FIRST_ANIM,()=>{
                this.setState({
                    chips:[]
                });
                this.resetSecondPageAnimations();
                this.startSecondPageAnimations();
            });
        },1000);
    }

    resetSecondPageAnimations(){
        this.refs.zoomlens.removeAttribute("class");
        this.refs.zoomlens.classList.add("zoomlens");
        this.setState({animateText:""})
        this.setState({chips:[]});
        window.clearTimeout(this.ct);
        window.clearInterval(this.cinterval);
    }

    componentWillUnmount(){
        window.clearTimeout(this.ct);
        window.clearInterval(this.cinterval);
    }
    
    render() {
        return (
            <div className="technology-wrap">
                <div className="pattern-bg"/>
                <div className="info-section">
                    <div className="container">
                        <div className="texte">
                            {this.state.animateText}
                            <div className="ts">
                                {this.state.animateText1}
                            </div>
                            <div className="cur-title">{this.state.currentTitle}</div>
                        </div>
                        <div className="post-card first right" ref="anim1">
                            <div ref="bgimage" className="img-holder" style={{"backgroundImage":"url(" + require("../../images/3.jpg") + ")"}}/>
                            <div className="zoomlens" ref="zoomlens"/>
                        </div>
                        <div className="normal-text" ref="anim2" style={{"display":"none"}}>
                            <div className="zoomlens dark" ref="zoomlens1"/>
                        </div>
                        <div className="chips-wrap">
                        {
                            this.state.chips.map((chipItem, index)=>{
                                return(
                                    <div className="chip" key={index}>
                                        <div className="ctext">{chipItem.text}</div>
                                        <div className="cicon">
                                            <img src={chipItem.image}/>
                                        </div>
                                        <div className="cpercent">{chipItem.percent}</div>
                                    </div>
                                );
                            })
                        }
                        </div>
                    </div>
                </div>
                <Footer />
            </div>
        );
    }
}