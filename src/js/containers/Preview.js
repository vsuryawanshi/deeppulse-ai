import React, {Component} from "react";
import ReactDOM from "react-dom";
import Footer from "../common/FooterComp";
import Typed from 'typed.js';
const DEMO_DATA = [
    {
        imgurl:require("../../images/privatebeach.jpg"),
        imgdesc: "Visually entice user in Poland during cold winter season",
        help_position:"top",
        heading:"Escape winter to Bali’s white beaches",
        items:[
            {
                pic:require("../../images/sun-umbrella.svg"),
                desc:"Private beach",
                help_text:"Highlighting white beaches because these are not available in Warsaw",
                help_position:"left"
            },
            {
                pic:require("../../images/surf.svg"),
                desc:"Special Christmas and New Year’s Eve package",
                help_text:"As context is December, focus on seasonal offer",
                help_position:"right"
            },
            {
                pic:require("../../images/food.svg"),
                desc:"European cuisine",
                help_text:"As user is from Poland, they might prefer European cuisine",
                help_position:"bottom"
            }
        ]
    },
    {
        imgurl:require("../../images/3.jpg"),
        imgdesc: "Set a romantic mood with the right personalized visual",
        help_position:"top",
        heading:"Indulge in romance with your loved one",
        items:[
            {
                pic:require("../../images/candles.svg"),
                desc:"Romantic 5-course dinner",
                help_text:"Complimentary ideas for couples’ trip",
                help_position:"left"
            },
            {
                pic:require("../../images/step-ladder.svg"),
                desc:"Adults only",
                help_text:"Enjoy peace and privacy without kids",
                help_position:"right"
            },
            {
                pic:require("../../images/massage.svg"),
                desc:"Couple SPA packages",
                help_text:"Highlight offers for couples",
                help_position:"bottom"
            }
        ]
    }
]
export default class PreviewPage extends Component{
    constructor(props){
        super(props);
        this.state = {
            cardData:null
        };
    }
    windowLoadedCompletely(){
		var loaderElem = document.getElementById("loader-wrap");
		loaderElem.style.display = "none";
    }
    
	componentDidMount() {
        this.windowLoadedCompletely();
        this.showProductDemo();
    }

    startTyping(){
        if(this.typed == null){
            this.firstTyping();   
        } else {
            this.typed.reset();
        }
    }
    firstTyping(){
        this.setState({cardData:null});
        if(this.typed){
            this.typed.destroy();
        }
        this.typed = new Typed(".typer1", {
            strings: ["Someone is looking at your hotel website from Warsaw in December"],
            typeSpeed: 40,
            onComplete:()=>{
                this.to = setTimeout(()=>{
                    this.setState({cardData:DEMO_DATA[0]});
                    this.to = setTimeout(()=>{
                        $('#product1').chardinJs('start');
                        this.to = setTimeout(() => {
                            $('#product1').chardinJs('stop');
                            this.secondTyping();
                        }, 10000);
                    },4000)
                },1000);
            }
        });
    }

    secondTyping(){
        if(this.typed){
            this.typed.destroy();
        }
        this.setState({cardData:null})
        this.typed = new Typed(".typer1",{
            strings: ["Someone landed on your website from a Google search seeking hotels for couples"],
            typeSpeed: 40,
            onComplete:()=>{
                this.to = setTimeout(()=>{
                    this.setState({cardData:DEMO_DATA[1]});
                    this.to = setTimeout(()=>{
                        $('#product1').chardinJs('start');
                        this.to = setTimeout(() => {
                            $('#product1').chardinJs('stop');
                            this.firstTyping();
                        }, 10000);
                    },4000)
                },1000);
            }
        });
    }

    resetTyping(){
        clearTimeout(this.to);
        if(this.typed){
            this.typed.destroy();
        }
        $('#product1').chardinJs('stop');
        this.setState({cardData:null})
    }

    showProductDemo(){
        ReactDOM.findDOMNode(this.refs.pdemo).style.display = "block";
        this.startTyping()
    }
    componentWillUnmount(){
        this.resetTyping();
    }

    render(){
        return(
            <div className="preview-wrap" id="product1">
                <div className="pattern-bg"/>
                <div className="width-container" ref="pdemo">
                    <div className="product-wrap">
                        <div className="product-demo">
                            <div className="search-box">
                                <span className="typer1"/>
                            </div>
                            <div className="card-wrap">
                                {
                                    this.state.cardData !== null ?
                                    <div className="carrd">
                                        <div className="ca-image" 
                                            style={{"backgroundImage":"url(" + this.state.cardData.imgurl + ")"}}
                                            data-intro={this.state.cardData.imgdesc} data-position={this.state.cardData.help_position}/>
                                        <div className="ca-content">
                                            <div className="ca-heading">{this.state.cardData.heading}</div>
                                            {
                                                this.state.cardData.items.map((item,index)=>{
                                                    return(
                                                        <div className="ca-desc" key={index}>
                                                            <img src={item.pic}/>
                                                            <span 
                                                                className="it-desc" 
                                                                data-intro={item.help_text}
                                                                data-position={item.help_position}>
                                                                {item.desc}
                                                            </span>
                                                        </div>
                                                    )
                                                })
                                            }
                                        </div>
                                    </div>
                                    :
                                    null
                                }
                            </div>
                        </div>
                    </div>
                </div>
				<Footer />
            </div>
        );
    }
}