import React, {Component} from "react";
import Footer from "../common/FooterComp";

export default class SolutionsPage extends Component{
    windowLoadedCompletely(){
		var loaderElem = document.getElementById("loader-wrap");
		loaderElem.style.display = "none";
		setTimeout(()=>{
			//this.startImageAnimation();
		},200);
	}
	componentDidMount() {
		//this.loadImageOnCanvas(require("../../images/3.jpg"));       
		this.windowLoadedCompletely();
	}
    render(){
        return(
            <div className="solutions-wrap" onClick={()=>{this.setState({showDropDown:false})}}>
				<div className="imgholder"/>
				<section className="product-info">
				    <div className="main-title">
                    Your customized DeepPulse digital sales agent, uses DNAfication technology which taps into six different data DNAs to derive powerful and personalized insights. Matching your customer needs to the exact property offerings. Converting them from website visitors to customers!!
                    </div>
                    <div className="sub-title" style={{display:"none"}}>
                        DeepPulse identifies your customer needs and highlights your offerings accordingly, providing real customer satisfaction and dramatically increasing conversions. DeepPulse uses DNAfication technology which taps into different data DNAs to derive powerful and personalized insights.
                    </div>
                    <div className="info-secs">
                        <div className="isec">
                            <div className="sec-ico">
                                <img src={require("../../images/fingerprint.svg")}/>
                            </div>
                            <div className="sec-head">
                                Automated DNAfication
                            </div>
                            <div className="sec-desc">
                                Identify customer persona and the search context to derive their needs
                            </div>
                        </div>
                        <div className="isec">
                            <div className="sec-ico">
                                <img src={require("../../images/insight.svg")}/>
                            </div>
                            <div className="sec-head">
                                Real-time insights
                            </div>
                            <div className="sec-desc">
                                Tailor property and location data to customer relevant highlights
                            </div>
                        </div>
                        <div className="isec">
                            <div className="sec-ico">
                                <img src={require("../../images/chat.svg")}/>
                            </div>
                            <div className="sec-head">
                                Personalized messaging
                            </div>
                            <div className="sec-desc">
                            Dynamically personalized and persuasive messaging for each customer
                            </div>
                        </div>
                    </div>
				</section>
				<Footer />
            </div>
        )
    }
}