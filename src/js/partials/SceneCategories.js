import React, { Component } from 'react';
import LinearProgress from "../common/LinearProgress";

export default class SceneCategories extends Component {
    render() {
        return (
            <div className="places card">
                <div className="card-inner">
                    <div className="chead">{this.props.tagData.displayName}</div>
                    {
                        this.props.tagData.tag[0] && this.props.tagData.tag[0].value.length > 0 ?
                        <div className="places-content">
                            {
                                this.props.tagData.tag[0].value.map((cat,index)=>{
                                    return(
                                        <div className="places-item" key={index}>
                                            <div className="cat-title">{cat.tag_name} <span className="vv">({parseFloat(cat.value*100).toFixed(1)}%)</span> </div>
                                            <LinearProgress value={cat.value*100}/>
                                        </div>
                                    )
                                })
                            }
                        </div>
                        :
                        <div className="generic-no">
                            No Scene categories identified
                        </div>
                    }
                </div>
            </div>
        );
    }
}