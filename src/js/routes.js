import React from 'react';
import {Route, IndexRoute } from 'react-router';

import Async from 'react-code-splitting'

const landing = () => <Async load={import(/* webpackChunkName: "landing" */'./containers/Landing')} />
const demo = () => <Async load={import(/* webpackChunkName: "demo" */'./containers/NewDemo')} />
const hotelpreviewone = () => <Async load={import(/* webpackChunkName: "demo" */'./containers/Preview')} />
const hotelpreviewtwo = () => <Async load={import(/* webpackChunkName: "demo" */'./containers/Preview.1')} />
const hotelpreviewthree = () => <Async load={import(/* webpackChunkName: "demo" */'./containers/Preview.4')} />
const rpreviewone = () => <Async load={import(/* webpackChunkName: "demo" */'./containers/Preview.2')} />
const rpreviewtwo = () => <Async load={import(/* webpackChunkName: "demo" */'./containers/Preview.3')} />
const solutions = () => <Async load={import(/* webpackChunkName: "solutions" */'./containers/Solutions')} />
const about = () => <Async load={import(/* webpackChunkName: "about" */'./containers/About')} />
const pricing = () => <Async load={import(/* webpackChunkName: "pricing" */'./containers/Pricing')} />
const technology1 = () => <Async load={import(/* webpackChunkName: "pricing" */'./containers/Technology')} />
const technology2 = () => <Async load={import(/* webpackChunkName: "pricing" */'./containers/Technology.1')} />
const technology3 = () => <Async load={import(/* webpackChunkName: "pricing" */'./containers/Technology.2')} />
const technology4 = () => <Async load={import(/* webpackChunkName: "pricing" */'./containers/Technology.3')} />
const terms = () => <Async load={import(/* webpackChunkName: "pricing" */'./containers/TermsConditions')} />
const privacy = () => <Async load={import(/* webpackChunkName: "pricing" */'./containers/Privacy')} />
const cookie = () => <Async load={import(/* webpackChunkName: "pricing" */'./containers/Cookie')} />
const signup = () => <Async load={import(/* webpackChunkName: "pricing" */'./containers/SignUp')} />
export default (
    <Route>
        <Route path="/" component={landing}/>
        <Route path="demo" component={demo}/>
        <Route path="hotelpreviewone" component={hotelpreviewone}/>
        <Route path="hotelpreviewtwo" component={hotelpreviewtwo}/>
        <Route path="hotelpreviewthree" component={hotelpreviewthree}/>
        <Route path="rpreviewone" component={rpreviewone}/>
        <Route path="rpreviewtwo" component={rpreviewtwo}/>
        <Route path="solutions" component={solutions}/>
        <Route path="about" component={about}/>
        <Route path="pricing" component={pricing}/>
        <Route path="cvtech" component={technology1}/>
        <Route path="nlptech" component={technology2}/>
        <Route path="personatech" component={technology3}/>
        <Route path="dnatech" component={technology4}/>
        <Route path="terms" component={terms}/>
        <Route path="privacy" component={privacy}/>
        <Route path="cookie" component={cookie}/>
        <Route path="contact" component={signup}/>
        <Route path="*" component={landing}/>
    </Route>
);