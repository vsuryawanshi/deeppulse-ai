import React from 'react';
import { render } from 'react-dom';
import '../styles/main.scss';
import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';
require("../favicon.ico");
import "./chardinjs";
import "./chardinjs.css";

import AppContainer from "./containers/App"

render(
    <AppContainer />,document.getElementById('appcontainer')
);