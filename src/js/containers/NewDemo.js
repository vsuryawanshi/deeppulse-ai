import React, {Component} from "react";
import Footer from "../common/FooterComp";

const INDUSTRIES=[
    "Hotel",
    "Travel",
    "Real Estate",
    "OTA"
];

export default class DemoPage extends Component{
    constructor(props){
        super(props);
        window.onload = () => {
            this.windowLoadedCompletely();
        };
        
        this.state={
            selectedIndustry:0
        }
    }
    
    windowLoadedCompletely(){
        var loaderElem = document.getElementById("loader-wrap");
        loaderElem.style.display = "none";
	}

    render(){
        return(
            <div className="demo-page">
                <div className="title">Select your industry</div>
                <div className="id-wrap">
                    {
                        INDUSTRIES.map((industry,index)=>{
                            return(
                                <div 
                                    className={`industry-btn` + (this.state.selectedIndustry == index ? " active" : "")}
                                    key={index}
                                    onClick={()=>{
                                        this.setState({
                                            selectedIndustry:index
                                        });
                                    }}>
                                    {industry}
                                </div>
                            );
                        })
                    }
                </div>
                <div className="cards-wrap">
                    <div className="card-item">
                        <div className="img-section" style={{backgroundImage:"url(http://62.210.100.210:8099/skyscraper.jpg)"}}/>
                        <div className="title-section">Manhattan Skyline</div>
                        <div className="content">
                            <div className="sec">
                                <span className="b">Scene Attributes </span>
                                man-made, vertical, natural-light, open-area, no-horizon
                            </div>
                            <div className="sec">
                                <span className="b">Scene Environment </span>
                                Open
                            </div>
                            <div className="sec">
                                <span className="b">Weather </span>
                                Cloudy
                            </div>
                        </div>
                    </div>

                    <div className="card-item">
                        <div className="img-section" style={{backgroundImage:"url(http://www.elitetraveler.com/wp-content/uploads/2013/08/Atlantis-The-Palm-4.jpg)"}}/>
                        <div className="title-section">Hotel Room</div>
                        <div className="content">
                            <div className="sec">
                                <span className="b">Scene Attributes </span>
                                no-horizon, enclosed area, man-made, wood, soothing
                            </div>
                            <div className="sec">
                                <span className="b">Scene Environment </span>
                                Indoor
                            </div>
                            <div className="sec">
                                <span className="b">Weather </span>
                                Cannot determine
                            </div>
                        </div>
                    </div>

                    <div className="card-item">
                        <div className="img-section" style={{backgroundImage:"url(https://i.imgur.com/Up4a1F3.jpg)"}}/>
                        <div className="title-section">Attending Desk</div>
                        <div className="content">
                            <div className="sec">
                                <span className="b">Scene Attributes </span>
                                no-horizon, enclosed area, attending desk, hotel
                            </div>
                            <div className="sec">
                                <span className="b">Scene Environment </span>
                                Indoor
                            </div>
                            <div className="sec">
                                <span className="b">Weather </span>
                                Cannot determine
                            </div>
                        </div>
                    </div>
                </div>
                <Footer />
            </div>
        )
    }
}