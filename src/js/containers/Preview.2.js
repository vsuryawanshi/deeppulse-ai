import React, {Component} from "react";
import ReactDOM from "react-dom";
import Footer from "../common/FooterComp";
import Typed from 'typed.js';
const DEMO_DATA = [
    {
        imgurl:require("../../images/demos/watermark.jpg"),
        imgdesc: "A property owner is trying to upload watermarked image",
        help_position:"top",
        heading:"Uploaded photo of a property by real estate agent",
        items:[
            {
                pic:require("../../images/sun-umbrella.svg"),
                desc:"Watermark",
                help_text:"DeepPulse engine detects that its a watermarked image",
                help_position:"left"
            }
        ]
    },
    {
        imgurl:require("../../images/demos/watermarklogo.jpg"),
        imgdesc: "A property owner is trying to upload watermarked image",
        help_position:"top",
        heading:"Uploaded photo of a property by real estate agent",
        items:[
            {
                pic:require("../../images/candles.svg"),
                desc:"Image with logo",
                help_text:"DeepPulse engine detects that this image contains a logo",
                help_position:"left"
            }
        ]
    }
]
export default class PreviewPage extends Component{
    constructor(props){
        super(props);
        this.state = {
            cardData:null
        };
    }
    windowLoadedCompletely(){
		var loaderElem = document.getElementById("loader-wrap");
		loaderElem.style.display = "none";
    }
    
	componentDidMount() {
        this.windowLoadedCompletely();
        this.showProductDemo();
    }

    startTyping(){
        if(this.typed == null){
            this.firstTyping();   
        } else {
            this.typed.reset();
        }
    }
    firstTyping(){
        this.setState({cardData:null});
        if(this.typed){
            this.typed.destroy();
        }
        this.typed = new Typed(".typer3", {
            strings: ["Watermark detector finds out whether watermark is present in the uploaded image"],
            typeSpeed: 40,
            onComplete:()=>{
                this.to = setTimeout(()=>{
                    this.setState({cardData:DEMO_DATA[0]});
                    this.to = setTimeout(()=>{
                        $('#product3').chardinJs('start');
                        this.to = setTimeout(() => {
                            $('#product3').chardinJs('stop');
                            this.secondTyping();
                        }, 10000);
                    },4000)
                },1000);
            }
        });
    }

    secondTyping(){
        if(this.typed){
            this.typed.destroy();
        }
        this.setState({cardData:null})
        this.typed = new Typed(".typer3",{
            strings: ["It also detects whether any logo is present in the image"],
            typeSpeed: 40,
            onComplete:()=>{
                this.to = setTimeout(()=>{
                    this.setState({cardData:DEMO_DATA[1]});
                    this.to = setTimeout(()=>{
                        $('#product').chardinJs('start');
                        this.to = setTimeout(() => {
                            $('#product').chardinJs('stop');
                            this.firstTyping();
                        }, 10000);
                    },4000)
                },1000);
            }
        });
    }

    resetTyping(){
        clearTimeout(this.to);
        if(this.typed){
            this.typed.destroy();
        }
        $('#product3').chardinJs('stop');
        this.setState({cardData:null})
    }

    showProductDemo(){
        ReactDOM.findDOMNode(this.refs.pdemo).style.display = "block";
        this.startTyping()
    }

    showPreviewInfo(){
        this.resetTyping();   
        ReactDOM.findDOMNode(this.refs.pdemo).style.display = "none";
    }

    componentWillUnmount(){
        this.resetTyping();    
    }

    render(){
        return(
            <div className="preview-wrap" id="product3">
                <div className="pattern-bg"/>
                <div className="width-container" ref="pdemo">
                    <div className="product-wrap">
                        <div className="product-demo">
                            <div className="search-box">
                                <span className="typer3"/>
                            </div>
                            <div className="card-wrap">
                                {
                                    this.state.cardData !== null ?
                                    <div className="carrd">
                                        <div className="ca-image" 
                                            style={{"backgroundImage":"url(" + this.state.cardData.imgurl + ")"}}
                                            data-intro={this.state.cardData.imgdesc} data-position={this.state.cardData.help_position}/>
                                        <div className="ca-content">
                                            <div className="ca-heading">{this.state.cardData.heading}</div>
                                            {
                                                this.state.cardData.items.map((item,index)=>{
                                                    return(
                                                        <div className="ca-desc" key={index}>
                                                            <span 
                                                                className="it-desc" 
                                                                data-intro={item.help_text}
                                                                data-position={item.help_position}>
                                                                {item.desc}
                                                            </span>
                                                        </div>
                                                    )
                                                })
                                            }
                                        </div>
                                    </div>
                                    :
                                    null
                                }
                            </div>
                        </div>
                    </div>
                </div>
				<Footer />
            </div>
        );
    }
}