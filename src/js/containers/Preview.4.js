import React, {Component} from "react";
import ReactDOM from "react-dom";
import Footer from "../common/FooterComp";
import Typed from 'typed.js';
const DEMO_DATA = [
    {
        imgurl:require("../../images/demos/badquality1.jpg"),
        imgdesc: "A very bad example of property bedroom photo",
        help_position:"top",
        heading:"Bedroom image of the property",
        items:[
            {
                pic:require("../../images/sun-umbrella.svg"),
                desc:"Blur",
                help_text:"It detects that the image is blurred",
                help_position:"left"
            },
            {
                pic:require("../../images/surf.svg"),
                desc:"Pixelated",
                help_text:"It also detects that image is pixelated",
                help_position:"right"
            },
            {
                pic:require("../../images/food.svg"),
                desc:"Inverted",
                help_text:"PhotoPulse also finds whether image orientation is correct or not",
                help_position:"bottom"
            }
        ]
    },
    {
        imgurl:require("../../images/demos/goodquality1.jpg"),
        imgdesc: "High quality property bedroom image",
        help_position:"top",
        heading:"Bedroom image of a real estate",
        items:[
            {
                pic:require("../../images/candles.svg"),
                desc:"High Quality",
                help_text:"PhotoPulse classifies the image as highly attractive image",
                help_position:"left"
            },
            {
                pic:require("../../images/step-ladder.svg"),
                desc:"Ocean view",
                help_text:"Ocean view is the most interesting feature of this image",
                help_position:"right"
            }
        ]
    }
]
export default class PreviewPageFour extends Component{
    constructor(props){
        super(props);
        this.state = {
            cardData:null
        };
    }
    windowLoadedCompletely(){
		var loaderElem = document.getElementById("loader-wrap");
		loaderElem.style.display = "none";
    }
    
	componentDidMount() {
        this.windowLoadedCompletely();
        this.showProductDemo();
    }

    startTyping(){
        if(this.typed == null){
            this.firstTyping();   
        } else {
            this.typed.reset();
        }
    }
    firstTyping(){
        this.setState({cardData:null});
        if(this.typed){
            this.typed.destroy();
        }
        this.typed = new Typed(".typer4", {
            strings: ["TopRank ranks the images based on their quality score"],
            typeSpeed: 40,
            onComplete:()=>{
                this.to = setTimeout(()=>{
                    ReactDOM.findDOMNode(this.refs.img1).style.display = "block";
                    this.to = setTimeout(() => {
                        ReactDOM.findDOMNode(this.refs.img2).style.display = "block";
                        this.to = setTimeout(() => {
                            ReactDOM.findDOMNode(this.refs.img3).style.display = "block";
                            this.to = setTimeout(()=>{
                                ReactDOM.findDOMNode(this.refs.img1).style.display = "none";
                                ReactDOM.findDOMNode(this.refs.img2).style.display = "none";
                                ReactDOM.findDOMNode(this.refs.img3).style.display = "none";
                                this.to = setTimeout(()=>{
                                    this.resetTyping();
                                    this.startTyping();
                                },2000);
                            },3000);
                        }, 2000);    
                    }, 2000);    
                    
                },2000);
            }
        });
    }

    secondTyping(){
        if(this.typed){
            this.typed.destroy();
        }
        this.setState({cardData:null})
        this.typed = new Typed(".typer4",{
            strings: ["Photo pulse also finds out interesting content from a property image"],
            typeSpeed: 40,
            onComplete:()=>{
                this.to = setTimeout(()=>{
                    this.setState({cardData:DEMO_DATA[1]});
                    this.to = setTimeout(()=>{
                        $('#product4').chardinJs('start');
                        this.to = setTimeout(() => {
                            $('#product4').chardinJs('stop');
                            this.firstTyping();
                        }, 10000);
                    },4000)
                },1000);
            }
        });
    }

    resetTyping(){
        clearTimeout(this.to);
        if(this.typed){
            this.typed.destroy();
        }
    }

    showProductDemo(){
        ReactDOM.findDOMNode(this.refs.pdemo).style.display = "block";
        this.startTyping()
    }

    showPreviewInfo(){
        this.resetTyping();   
        ReactDOM.findDOMNode(this.refs.pdemo).style.display = "none";
    }

    componentWillUnmount(){
        this.resetTyping();
    }

    render(){
        return(
            <div className="preview-wrap" id="product4">
                <div className="pattern-bg"/>
                <div className="width-container" ref="pdemo">
                    <div className="product-wrap">
                        <div className="product-demo">
                            <div className="search-box">
                                <span className="typer4"/>
                            </div>
                            <div className="card-wrap">
                                <div className="carrd" ref="img1" style={{display:"none"}}>
                                    <div className="ca-image" 
                                        style={{"backgroundImage":"url(" + require("../../images/demos/ranklow.jpg") + ")"}}/>
                                    <div className="ca-content">
                                        <div className="ca-heading vc">Low Quality Image</div>
                                    </div>
                                </div>
                                <div className="carrd" ref="img2" style={{display:"none"}}>
                                    <div className="ca-image" 
                                        style={{"backgroundImage":"url(" + require("../../images/demos/rankavg.png") + ")"}}/>
                                    <div className="ca-content">
                                        <div className="ca-heading vc">Average Quality Image</div>
                                    </div>
                                </div>
                                <div className="carrd" ref="img3" style={{display:"none"}}>
                                    <div className="ca-image" 
                                        style={{"backgroundImage":"url(" + require("../../images/demos/rankhigh.jpg") + ")"}}/>
                                    <div className="ca-content">
                                        <div className="ca-heading vc">High Quality Image</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
				<Footer />
            </div>
        );
    }
}