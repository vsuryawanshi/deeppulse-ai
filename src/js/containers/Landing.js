import React, { Component } from 'react';
import {findDOMNode} from "react-dom";
import { browserHistory } from 'react-router';
import ReactHtmlParser, { processNodes, convertNodeToElement, htmlparser2 } from 'react-html-parser';
import Footer from "../common/FooterComp";
// const ANIMATION_DATA = [
// 	{
// 		"text1":"Cutting-edge computer vision capabilities",
// 		"text2":"Mina analyzes hotel images through hundreds of different parameters, promising to present the right picture in the right context.",
// 		"image":require("../../images/anime1.png"),
// 		"tags":["Smiling woman","Smiling man","Burning candle","Lamp","Romantic scene"]
// 	},
// 	{
// 		"text1":"Built-in natural-language processing engine",
// 		"text2":"Gives us the capability to understand every review and user sentiments in minute detail.",
// 		"image":require("../../images/anime2.png"),
// 		"tags":["Drinking Illegal","Horrific Location","No Resort","Old furniture","Overall negative sentiments"]
// 	},
// 	{
// 		"text1":"Understands different persona",
// 		"text2":"Mina provides a one-to-one conversational experience with every individual, no matter their cultural background.",
// 		"image":require("../../images/anime3.jpg"),
// 		"tags":["Elevator","In-room service","Dietary restrictions","In premise medical amenities"]
// 	},
// 	{
// 		"text1":"Global location intelligence engine",
// 		"text2":"We understand the DNA of every location on earth and personalize every message sent to show some users features unavailable at their current location.",
// 		"image":require("../../images/anime4.jpg"),
// 		"tags":["Crowded city","Subway Available","Multicuisine food","No beach","No mountains","No snowfall"]
// 	}
// ]

export default class LandingPage extends Component {
	constructor(props){
		super(props);
		this.state = {
			showMobileMenu : false,
		};
	}

	windowLoadedCompletely(){
		var loaderElem = document.getElementById("loader-wrap");
		loaderElem.style.display = "none";
	}
	componentDidMount() {
		this.windowLoadedCompletely();
	}

	render() {
		return (
			<div className="landing-wrapper">
				<div className="pattern-bg"/>
				<section className="demo">
					<div className="holder">
						<div className="intro-text">
							<div className="heading1">An AI powered sales assistant for your digital businesses</div>
							<div className="heading3">Bring the pursuance power of sales agent to your online product</div>
							<div className="started-btn" onClick={()=>{
								browserHistory.push("contact");
							}}>
								<span>Get your assistant now</span>
							</div>
						</div>
						<div className="image-demo">
							<div className="image-box"/>
						</div>
					</div>
				</section>
				<section className="solution-content">
					<div className="main-title">With AI, tailored to your business. Convert lookers to bookers!</div>
					<div className="solution-wrap">
						<div className="solution-item">
							<img src={require("../../images/spamdetection.svg")} className="sol-img"/>
							<div className="sol-heading">Content Filtering</div>
							<div className="sol-content">Detect and remove the irrelevant content for a customer</div>
						</div>
						<div className="solution-item">
							<img src={require("../../images/autotagging.svg")} className="sol-img"/>
							<div className="sol-heading">Augment Features</div>
							<div className="sol-content">Add relevant additional information to your product description</div>
						</div>
						<div className="solution-item">
							<img src={require("../../images/personamatching.svg")} className="sol-img"/>
							<div className="sol-heading">Message of one</div>
							<div className="sol-content">Dynamically personalized messaging for each customer</div>
						</div>
						<div className="solution-item">
							<img src={require("../../images/deepfeature.svg")} className="sol-img"/>
							<div className="sol-heading">Know your customer</div>
							<div className="sol-content">Derive your customer needs with minimal intervention</div>
						</div>
					</div>
				</section>
				<Footer />
			</div>
		);
	}
}