import React, { Component } from 'react';
import ReactDOM from "react-dom";
import {FIRST_ANIM, SECOND_ANIM, THIRD_ANIM, FOURTH_ANIM} from "../common/constants";
import $ from "jquery";
import Footer from "../common/FooterComp";

export default class MinaTechComp extends Component {
    constructor(props){
        super(props);
        this.state = {
            chips :[],
            animateText:"Built-in natural-language processing engine",
            animateText1:"gives us the capability to understand every review and user sentiments in minute detail.",
            currentTitle:"Understands even minute details"
        };
    }

    windowLoadedCompletely(){
		var loaderElem = document.getElementById("loader-wrap");
		loaderElem.style.display = "none";
    }
    
    componentDidMount() {
        this.windowLoadedCompletely();
        this.moveToSecondImage();
    }

    extractAndShowChips(arr, callback){
        var loopIndex = 1;
        this.cinterval = window.setInterval(()=>{
            this.setState({chips:[]});
            var slicedArray = arr.slice(0,loopIndex++);
            this.setState({chips:slicedArray});
            if(loopIndex == arr.length + 1){
                clearInterval(this.cinterval);
                setTimeout(()=>{
                    callback();
                },2000);
            }
        },1000);   
    }

    moveToSecondImage(){
        var a2 = ReactDOM.findDOMNode(this.refs.anim2)
        a2.style.display = "flex";
        this.ct = setTimeout(()=>{
            ReactDOM.findDOMNode(this.refs.zoomlens1).classList.add("path2");
            var topPosition = $(a2).position().top + $(a2).height() + 70;
            $(".chips-wrap").css({top:topPosition+"px"});
            this.extractAndShowChips(SECOND_ANIM,()=>{
                this.setState({
                    chips:[]
                });
                this.resetSecondPageAnimations();
                this.moveToSecondImage();
            });
        },1000)
    }

    resetSecondPageAnimations(){
        this.setState({chips:[]});
        window.clearTimeout(this.ct);
        ReactDOM.findDOMNode(this.refs.zoomlens1).classList.remove("path2");
        window.clearInterval(this.cinterval);
    }

    componentWillUnmount(){
        window.clearTimeout(this.ct);
        window.clearInterval(this.cinterval);
    }
    render() {
        return (
            <div className="technology-wrap">
                <div className="pattern-bg"/>
                <div className="info-section">
                    <div className="container">
                        <div className="texte">
                            {this.state.animateText}
                            <div className="ts">
                                {this.state.animateText1}
                            </div>
                            <div className="cur-title">{this.state.currentTitle}</div>
                        </div>
                        <div className="normal-text" ref="anim2" style={{"display":"none"}}>
                            <div className="zoomlens dark" ref="zoomlens1"/>
                            <div className="review-page">
                                <div className="blue-title"/>
                                <div className="content-wrap">
                                    <div className="content-pic">
                                        <img src={require("../../images/review-pic.png")}/>
                                    </div>
                                    <div className="content-text">
                                        <div className="cheading">
                                            Dirty room...unsafe outdoors, really bad experience!    
                                        </div>
                                        <p>Wow, I've never needed a drink so badly and it is illegal there.</p>
                                        <p>The area is horrific, literally a rubbish dump packed with many shady characters on motorbikes hanging around in packs.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="chips-wrap">
                        {
                            this.state.chips.map((chipItem, index)=>{
                                return(
                                    <div className="chip" key={index}>
                                        <div className="ctext">{chipItem.text}</div>
                                        <div className="cicon">
                                            <img src={chipItem.image}/>
                                        </div>
                                        <div className="cpercent">{chipItem.percent}</div>
                                    </div>
                                );
                            })
                        }
                        </div>
                    </div>
                </div>
                <Footer />
            </div>
        );
    }
}