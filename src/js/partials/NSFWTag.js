import React, { Component } from 'react';
import LinearProgress from "../common/LinearProgress";

export default class NSFWTag extends Component {
    getProgressValue(value){
        return (parseFloat(value)*100).toFixed(3);
    }

    render() {
        return (
            <div className="nsfw card">
                <div className="card-inner">
                    <div className="chead">{this.props.tagData[0].displayName}</div>
                    <div className="nsfw-content">
                        {
                            this.props.tagData[0].tag[0] && this.props.tagData[0].tag[0].value != ""?
                            <div className="nsfw-wrap">
                                <div className="nsfw-lbl">
                                    {this.props.tagData[0].tag[0].value}
                                </div>
                                {
                                    Object.keys(this.props.tagData[0].attrs[0]).map((attr,index)=>{
                                        let attrProg = this.getProgressValue(this.props.tagData[0].attrs[0][attr]);
                                        return(
                                            <div className="attr-item" key={index}>
                                                <div className="top-wrap">
                                                    <div className="prop-name">{attr}</div>
                                                    <div className="prop-value">
                                                        <LinearProgress value={attrProg}/>
                                                    </div>
                                                </div>
                                                <div className="perc">({attrProg}%)</div>
                                            </div>
                                        )
                                    })
                                }
                            </div>
                            :
                            <div className="generic-no">
                                Could not determine
                            </div>
                        }
                    </div>  
                </div>
                
            </div>
        );
    }
}