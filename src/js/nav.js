export default {
    items: [
        {
            name: 'Preview',
            url: 'preview',
            submenu:[
                {
                    name:"Hotel Booking",
                    url:""
                },
                {
                    name:"Mina - The Sales Wizard",
                    url:"hotelpreviewone",
                    type:"submenu"
                },
                {
                    name:"PhotoPulse",
                    url:"hotelpreviewtwo",
                    type:"submenu"
                },
                {
                    name:"TopRank",
                    url:"hotelpreviewthree",
                    type:"submenu"
                },
                {
                    name:"Real Estate",
                    url:""
                },
                {
                    name:"Watermark Detector",
                    url:"rpreviewone",
                    type:"submenu"
                },
                {
                    name:"PhotoPulse",
                    url:"rpreviewtwo",
                    type:"submenu"
                }
            ]
        },
        {
            name: 'Solutions',
            url: 'solutions',
        },
        {
            name: 'Company',
            url: 'about'
        },
        {
            name: 'Technology',
            url: 'technology',
            submenu:[
                {
                    name:"Computer Vision",
                    url:"cvtech"
                },
                {
                    name:"NLP",
                    url:"nlptech"
                },
                {
                    name:"Personification",
                    url:"personatech"
                },
                {
                    name:"DNAFication",
                    url:"dnatech"
                }
            ]
        }
    ]
};