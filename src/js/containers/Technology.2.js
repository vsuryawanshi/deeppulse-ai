import React, { Component } from 'react';
import ReactDOM from "react-dom";
import {FIRST_ANIM, SECOND_ANIM, THIRD_ANIM, FOURTH_ANIM} from "../common/constants";
import $ from "jquery";
import Footer from "../common/FooterComp";

export default class MinaTechComp extends Component {
    constructor(props){
        super(props);
        this.state = {
            chips :[],
            animateText:"Our operating system understands different personas",
            animateText1:"For meeting their personalized needs and providing an one-to-one conversational experience, no matter what their cultural backgrounds are",
            currentTitle:"Understands different personas effectively"
        };
    }

    windowLoadedCompletely(){
		var loaderElem = document.getElementById("loader-wrap");
		loaderElem.style.display = "none";
    }
    
    componentDidMount() {
        this.windowLoadedCompletely();
        this.moveToThirdImage();
    }

    extractAndShowChips(arr, callback){
        var loopIndex = 1;
        this.cinterval = window.setInterval(()=>{
            this.setState({chips:[]});
            var slicedArray = arr.slice(0,loopIndex++);
            this.setState({chips:slicedArray});
            if(loopIndex == arr.length + 1){
                clearInterval(this.cinterval);
                setTimeout(()=>{
                    callback();
                },2000);
            }
        },1000);   
    }

    moveToThirdImage(){
        var a3 = ReactDOM.findDOMNode(this.refs.anim3);
        a3.style.display = "block";
        this.ct = setTimeout(()=>{ 
            ReactDOM.findDOMNode(this.refs.zoomlens2).classList.add("path3");
            var topPosition = $(a3).position().top + $(a3).height() + 70;
            $(".chips-wrap").css({top:topPosition+"px"});
            this.extractAndShowChips(THIRD_ANIM, ()=>{
                this.setState({
                    chips:[]
                });
                this.resetSecondPageAnimations();
                this.moveToThirdImage();
            });
        },1000)
    }

    resetSecondPageAnimations(){
        this.setState({chips:[]});
        window.clearTimeout(this.ct);
        ReactDOM.findDOMNode(this.refs.zoomlens2).classList.remove("path3");
        window.clearInterval(this.cinterval);
    }

    componentWillUnmount(){
        window.clearTimeout(this.ct);
        window.clearInterval(this.cinterval);
    }
    render() {
        return (
            <div className="technology-wrap">
                <div className="pattern-bg"/>
                <div className="info-section">
                    <div className="container">
                        <div className="texte">
                            {this.state.animateText}
                            <div className="ts">
                                {this.state.animateText1}
                            </div>
                            <div className="cur-title">{this.state.currentTitle}</div>
                        </div>
                        <div className="t-img" ref="anim3">
                            <div className="zoomlens dark" ref="zoomlens2"/>
                                <img src={require("../../images/old-man-standing.png")}/>
                                <img src={require("../../images/old-woman-standing.png")}/>
                        </div>
                        <div className="chips-wrap">
                        {
                            this.state.chips.map((chipItem, index)=>{
                                return(
                                    <div className="chip" key={index}>
                                        <div className="ctext">{chipItem.text}</div>
                                        <div className="cicon">
                                            <img src={chipItem.image}/>
                                        </div>
                                        <div className="cpercent">{chipItem.percent}</div>
                                    </div>
                                );
                            })
                        }
                        </div>
                    </div>
                </div>
                <Footer />
            </div>
        );
    }
}