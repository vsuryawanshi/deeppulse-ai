import React, {Component} from "react";
import Footer from "../common/FooterComp";

const PEOPLE = [
    {
        name:"Ankur Goel",
        designation:"Founder",
        pic:require("../../images/team/ankur.jpg"),
        description:"An avid technologist, with over 10 years of experience working on AI, ML and analytics. Having worked on the PARAM series of computers, published a cookbook on graph database and provided technology firepower to several startups in the Silicon Valley and India. Ankur had his calling with DeepPulse! "
    },
    {
        name:"Pranav Maheshwari",
        designation:"Advisor",
        pic:require("../../images/team/pranav.jpg"),
        description:"An alumnus of Carnegie Mellon University and currently working in Silicon Valley. Pranav has a keen interest and a passion for artificial intelligence and robotics. He sits on the advisory board of DeepPulse, sharing his knowledge and vision, in the constantly changing AI landscape."
    },
    {
        name:"Jaisekhar",
        designation:"Chief Business Officer",
        pic:require("../../images/team/jaisekhar.jpg"),
        description:"A banker with two decades of experience in banking and FS institutions at management and strategic level positions. He now mentor’s startups in the AI space. Providing them with thought leadership and strategic guidance in their initial phases."
    },
    {
        name:"Rahul",
        designation:"Full stack developer",
        pic:require("../../images/team/rahul.jpg"),
        description:"Loves putting order to randomness using multiple trajectory of thoughts. Passionate about finding answers to difficult questions Rahul is the data forensic genius in picking subtle evidences, clues and conclusions. Startup enthusiast who loves exploring new technological tools."
    },
    {
        name:"Ankit",
        designation:"Technology Scientist",
        pic:require("../../images/team/ankit.jpeg"),
        description:"A vastly experienced backend engineer with experience in lots of backend technologies, Ankit has more than 9 years of experience in the IT industry. He also loves to travel to different places whenever he can."
    },
    {
        name:"Varun",
        designation:" Mobile and Front End Engineer",
        pic:require("../../images/team/varun.jpg"),
        description:"Varun loves working on delivering best user experience, be it on Web or on mobile. He has not limited himself in terms of technologies being used. He likes to draw occasionally."
    },
    {
        name:"Nandita",
        designation:"Business Operations",
        pic:require("../../images/team/nandita.jpg"),
        description:"An IT professional with 11 years of experience in various technical and functional roles like Application Development, Practice Enablement, Sales Enablement, Project Management, Business Operations, Sales Operations and Data Science. Nandita is currently a Data Scientist and an AI & Deep Learning Enthusiast."
    },
    {
        name:"Abhishek",
        designation:"Quality Assurer",
        pic:require("../../images/team/abhishek.jpeg"),
        description:"An 'Eager Tester' , a curious observer and a passionate traveller ! Highly specialised in performance , security and API testing and eager to contribute in the sphere of Artificial Intelligence. Having a strong driving force for the system to be of a high quality when it reaches the end user."
    },
    {
        name:"Rinku",
        designation:"Backend Developer",
        pic:require("../../images/team/rinku.jpeg"),
        description:"A core technologist, with knowledge on almost all technologies of Java, Rinku loves spending his time listening to music, playing cricket and talking to new people"
    },
    {
        name:"Diogo",
        designation:"Front End Engineer",
        pic:require("../../images/team/diogo.jpg"),
        description:"A passionate guy for any front end technology, Diogo has been in the industry for more than 1 year now, although he started very early learning new technologies"
    },
    {
        name:"Shakti",
        designation:"Technical Architect",
        pic:require("../../images/team/shakti.jpeg"),
        description:"Having more than 11 years of experience in IT industry with having good experience in designing architecture as well as developing critical requirements in Java/j2ee based applications. Have strong passion for technology and always willing to learn"
    },
    {
        name:"Gaurav",
        designation:"Image Processing and Data Scientist",
        pic:require("../../images/team/gaurav.jpg"),
        description:"Business Analyst with 6 years extensive experience in Business Analysis, Practice Enablement, Project Management, Pre-sales and Data Science. Data Science enthusiast and developer of AI applications in deep learning and image processing."
    },
    {
        name:"Sahil",
        designation:"Web Crawling, Backend Engineering",
        pic:require("../../images/team/sahil.jpg"),
        description:"Currently a btech information technology student having skills of web development, basically  frontend and crawling the websites using various tactics."
    },
    {
        name:"Prithavi",
        designation:"Web Crawling, Backend Engineering",
        pic:require("../../images/team/prithavi.jpg"),
        description:"An enthusiastic undergraduate from NIT Jalandhar having comendable skills in Web Development and Web Crawling. He loves watching TV Series and learning from Youtube videos."
    },
    {
        name:"Kaoutar El Fihri",
        designation:"Sales",
        pic:require("../../images/team/kaoutar.jpg")
    },
    {
        name:"Anuj",
        designation:"Image Processing and Backend",
        pic:require("../../images/team/anuj.jpg"),
        description:"A Computer Vision Researcher who has 9 yrs of Industry Experience. Developed solutions using Keras, Python and Tensorflow."
    },
    {
        name:"Talwinder",
        designation:"Full Stack Developer",
        pic:require("../../images/team/talwinder.jpg"),
        description:"Jack of all techs. Inquistive to explore different areas. Have also done machine learning and nlp"
    }
];
export default class AboutPage extends Component{
    constructor(props){
        super(props);

        this.state = {
            showModal:false,
            currentPerson:PEOPLE[0]
        };
    }
    windowLoadedCompletely(){
		var loaderElem = document.getElementById("loader-wrap");
		loaderElem.style.display = "none";
		setTimeout(()=>{
			//this.startImageAnimation();
		},200);
	}
	componentDidMount() {
		//this.loadImageOnCanvas(require("../../images/3.jpg"));       
		this.windowLoadedCompletely();
	}
    render(){
        return(
            <div className="about-wrapper">
                <section className="first">
                    <div className="pmask">
                        <div className="title">WHO WE ARE</div>
                        <div className="stitle">NOT JUST ANY BUNCH OF TECHNOLOGY ENTHUSIASTS</div>
                    </div>
                </section>
                <section className="second">
                    <div className="atitle">
                        ABOUT US
                    </div>
                    <div className="line"/>
                    <div className="about-content">
                        We are a young and hungry tech venture, integrating cutting edge technologies to transform sales on digital platforms. Converting website visitors into happy customers!!
                    </div>
                </section>
                <section className="third">
                    <div className="etitle">OUR FOUNDING TEAM</div>
                    <div className="line"/>
                    <div className="people-wrap">
                        {
                            PEOPLE.map((person,index)=>{
                                return(
                                    <div className="person" key={index} onClick={()=>{
                                        this.setState({
                                            currentPerson:person,
                                            showModal:true
                                        });
                                    }}>
                                        <div className="pic" style={{backgroundImage:"url(" + person.pic + ")"}}/>
                                        <div className="fright">
                                            <div className="front">
                                                <div className="name">{person.name}</div>
                                                <div className="desig">{person.designation}</div>
                                                <div className="llink">Read Bio</div>
                                            </div>
                                        </div>
                                    </div>
                                )
                            })
                        }
                    </div>
                </section>
                <section className="fourth">
                    <div className="fmask">
                        <div className="ftitle">WE WORK TOGETHER</div>
                    </div>
                </section>
                <section className="five">
                    <div className="ctitle">JOIN US</div>
                    <div className='cline'/>
                    <div className="ccontent">
                        <div className="sec-one">
                            <div className="about-sec">
                                <img src={require("../../images/team.svg")} className="about-img"/>
                                <div className="sec-desc">
                                    10+ dedicated people   
                                </div>     
                            </div>
                            <div className="about-sec">
                                <img src={require("../../images/world1.svg")} className="about-img"/>
                                <div className="sec-desc">
                                    Europe, Asia and the US
                                </div>     
                            </div>
                        </div>
                        <div className="sec-one">
                            <div className="about-sec">
                                <img src={require("../../images/innovate.svg")} className="about-img"/>
                                <div className="sec-desc">
                                    Passion for technology and innovation
                                </div>     
                            </div>
                            <div className="about-sec">
                                <img src={require("../../images/travel.svg")} className="about-img"/>
                                <div className="sec-desc">
                                    Focus on travel and hospitality
                                </div>     
                            </div>
                        </div>
                    </div>
                    <div className="ctitle">LET'S TALK</div>
                    <div className='cline'/>
                    <div className="email-btn" onClick={()=>{
                        window.location = "mailto:ankur@deeppulse.ai";
                    }}>
                        EMAIL-US
                    </div>
                </section>
                <Footer/>
                <div className={`about-modal-wrap` + (!this.state.showModal ? " hide" : "")} onClick={()=>{
                    this.setState({
                        showModal:false
                    });
                }}>
                    <div className="modal-container" onClick={(e)=>{
                        e.stopPropagation();
                    }}>
                        <img src={require("../../images/close.svg")} className="close-btn" onClick={()=>{
                            this.setState({
                                showModal:false
                            });
                        }}/>
                        <div className="first-container">
                            <div className="profile-pic" style={{backgroundImage:"url(" + this.state.currentPerson.pic +")"}}/>
                            <div className="per-info">
                                <div className="per-name">
                                    {this.state.currentPerson.name}
                                </div>
                                <div className="per-desig">
                                    {this.state.currentPerson.designation}
                                </div>
                            </div>
                        </div> 
                        <div className="per-desc">
                            {this.state.currentPerson.description}
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}