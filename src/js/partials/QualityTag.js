import React, { Component } from 'react';
import LinearProgress from "../common/LinearProgress";

export default class QualityTag extends Component {

    getScoreValue(item){
        var finalScore = 0;
        switch(item.DP_NAME){
            case "DP_QUA_AES":
                finalScore = (parseFloat(item.score) * 100).toFixed(2);
                break;
            default:
                finalScore = parseFloat(item.score).toFixed(2);
                break;
        }       
        return finalScore;
    }

    render() {
        return (
            <div className="qltyw card" style={this.props.style}>
                <div className="card-inner">
                    <div className="chead">Quality</div>
                    <div className="qlty-content">
                        {
                            this.props.tagData.map((tagItem,index)=>{
                                if(tagItem.tag.length > 0 && tagItem.DP_NAME != "DP_QUA_RES"){
                                    var val = this.getScoreValue(tagItem);
                                    return(
                                        <div className="item-row" key={index}>
                                            <div className="item-label">{tagItem.displayName}</div>
                                            <LinearProgress value={val}/>
                                            <div className="item-value">
                                                {tagItem.tag[0].value}
                                            </div>
                                        </div>
                                    )
                                } else {
                                    return null;
                                }
                            })
                        }
                    </div>
                </div>
            </div>
        );
    }
}