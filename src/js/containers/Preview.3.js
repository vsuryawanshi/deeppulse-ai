import React, {Component} from "react";
import ReactDOM from "react-dom";
import Footer from "../common/FooterComp";
import Typed from 'typed.js';
const DEMO_DATA = [
    {
        imgurl:require("../../images/demos/badquality1.jpg"),
        imgdesc: "A very bad example of property bedroom photo",
        help_position:"top",
        heading:"Bedroom image of the property",
        items:[
            {
                pic:require("../../images/sun-umbrella.svg"),
                desc:"Blur",
                help_text:"It detects that the image is blurred",
                help_position:"left"
            },
            {
                pic:require("../../images/surf.svg"),
                desc:"Pixelated",
                help_text:"It also detects that image is pixelated",
                help_position:"right"
            }
        ]
    },
    {
        imgurl:require("../../images/demos/goodquality1.jpg"),
        imgdesc: "High quality property exterior image",
        help_position:"top",
        heading:"A very high quality image of a property",
        items:[
            {
                pic:require("../../images/candles.svg"),
                desc:"High Quality",
                help_text:"PhotoPulse classifies the image as highly attractive image",
                help_position:"left"
            },
            {
                pic:require("../../images/step-ladder.svg"),
                desc:"Pool view",
                help_text:"Pool view is the most interesting feature of this image",
                help_position:"right"
            }
        ]
    }
]
export default class PreviewPage extends Component{
    constructor(props){
        super(props);
        this.state = {
            cardData:null
        };
    }
    windowLoadedCompletely(){
		var loaderElem = document.getElementById("loader-wrap");
		loaderElem.style.display = "none";
    }
    
	componentDidMount() {
        this.windowLoadedCompletely();
        this.showProductDemo();
    }

    startTyping(){
        if(this.typed == null){
            this.firstTyping();   
        } else {
            this.typed.reset();
        }
    }
    firstTyping(){
        this.setState({cardData:null});
        if(this.typed){
            this.typed.destroy();
        }
        this.typed = new Typed(".typer4", {
            strings: ["PhotoPulse detects quality parameters of a property image"],
            typeSpeed: 40,
            onComplete:()=>{
                this.to = setTimeout(()=>{
                    this.setState({cardData:DEMO_DATA[0]});
                    this.to = setTimeout(()=>{
                        $('#product4').chardinJs('start');
                        this.to = setTimeout(() => {
                            $('#product4').chardinJs('stop');
                            this.secondTyping();
                        }, 10000);
                    },4000)
                },1000);
            }
        });
    }

    secondTyping(){
        if(this.typed){
            this.typed.destroy();
        }
        this.setState({cardData:null})
        this.typed = new Typed(".typer4",{
            strings: ["Photo pulse also finds out interesting content from a property image"],
            typeSpeed: 40,
            onComplete:()=>{
                this.to = setTimeout(()=>{
                    this.setState({cardData:DEMO_DATA[1]});
                    this.to = setTimeout(()=>{
                        $('#product4').chardinJs('start');
                        this.to = setTimeout(() => {
                            $('#product4').chardinJs('stop');
                            this.firstTyping();
                        }, 10000);
                    },4000)
                },1000);
            }
        });
    }

    resetTyping(){
        clearTimeout(this.to);
        if(this.typed){
            this.typed.destroy();
        }
        $('#product4').chardinJs('stop');
        this.setState({cardData:null})
    }

    showProductDemo(){
        ReactDOM.findDOMNode(this.refs.pdemo).style.display = "block";
        this.startTyping()
    }

    showPreviewInfo(){
        this.resetTyping();   
        ReactDOM.findDOMNode(this.refs.pdemo).style.display = "none";
    }

    componentWillUnmount(){
        this.resetTyping();
    }

    render(){
        return(
            <div className="preview-wrap" id="product4">
                <div className="pattern-bg"/>
                <div className="width-container" ref="pdemo">
                    <div className="product-wrap">
                        <div className="product-demo">
                            <div className="search-box">
                                <span className="typer4"/>
                            </div>
                            <div className="card-wrap">
                                {
                                    this.state.cardData !== null ?
                                    <div className="carrd">
                                        <div className="ca-image" 
                                            style={{"backgroundImage":"url(" + this.state.cardData.imgurl + ")"}}
                                            data-intro={this.state.cardData.imgdesc} data-position={this.state.cardData.help_position}/>
                                        <div className="ca-content">
                                            <div className="ca-heading">{this.state.cardData.heading}</div>
                                            {
                                                this.state.cardData.items.map((item,index)=>{
                                                    return(
                                                        <div className="ca-desc" key={index}>
                                                            <span 
                                                                className="it-desc" 
                                                                data-intro={item.help_text}
                                                                data-position={item.help_position}>
                                                                {item.desc}
                                                            </span>
                                                        </div>
                                                    )
                                                })
                                            }
                                        </div>
                                    </div>
                                    :
                                    null
                                }
                            </div>
                        </div>
                    </div>
                </div>
				<Footer />
            </div>
        );
    }
}