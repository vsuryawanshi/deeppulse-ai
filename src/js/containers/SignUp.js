import React, {Component} from "react";
import axios from "axios";

const EMAIL_REGEX = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
export default class SignUpPage extends Component{
    constructor(props){
        super(props);
        this.state= {
            userName:"",
            userEmail:"",
            userMessage:"",
            userEmailError:"",
            userNameError:"",
            successMessage:"",
            apiCallInProgress:false,
            showSuccessMessage:false
        }
    }

    windowLoadedCompletely(){
		var loaderElem = document.getElementById("loader-wrap");
		loaderElem.style.display = "none";
		setTimeout(()=>{
			//this.startImageAnimation();
		},200);
	}
	componentDidMount() {
		//this.loadImageOnCanvas(require("../../images/3.jpg"));       
		this.windowLoadedCompletely();
    }
    
    sendEmail(){
        if(this.state.userEmail == ""){
            this.setState({
                userEmailError:"Please enter the email"
            });
        }
        if(this.state.userName == "") {
            this.setState({
                userNameError:"Please enter the username"
            });
        }
        if(!EMAIL_REGEX.test(this.state.userEmail)){
            this.setState({userEmailError:"Please enter a valid email"});
        }
        if(this.state.userEmail !== "" && this.state.userName !== ""){
            this.setState({
                apiCallInProgress:true
            },()=>{
                axios({
                    method:"POST",
                    url:"http://128.199.172.82:8090/user",
                    data:{
                        name:this.state.userName,
                        email:this.state.userEmail,
                        message:this.state.userMessage
                    }
                }).then((response)=>{
                    this.setState({
                        userEmail:"",
                        userName:"",
                        userMessage:"",
                        apiCallInProgress:false,
                        showSuccessMessage:true
                    },()=>{
                        setTimeout(()=>{
                            this.setState({
                                showSuccessMessage:false
                            })
                        },4000)
                    })
                }).catch((err)=>{
                    console.log(err);
                    this.setState({
                        userEmail:"",
                        userName:"",
                        userMessage:"",
                        apiCallInProgress:false
                    });
                })
            });
        }
    }

    render(){
        return(
            <div className="signup-wrapper">
                <div className={`success-modal-wrap` + (this.state.showSuccessMessage ? " show" : "")}>
                    <div className="success-modal">
                        <div className="msg">
                            Thanks for your interest and getting in touch with us. Our team should be in touch with you shortly.
                        </div>
                        <div className="ok-btn" onClick={()=>{
                            this.setState({
                                showSuccessMessage:false
                            });
                        }}>OK</div>
                    </div>
                </div>
                <div className="pattern-bg"/>
                <section className="first">
                    <div className="pmask">
                        <div className="title">CONTACT US</div>
                        <div className="stitle">Let us lead you into future of Digital Sales</div>
                    </div>
                </section>
                <section className="second">
                    <div className="gettouch">Get in touch with us</div>

                    <form name="contact-form" onSubmit={(e)=>{
                        e.preventDefault();
                        e.stopPropagation();
                        this.sendEmail();
                    }}>
                        <input 
                            className="custom-input big" 
                            type="email" 
                            placeholder="youremail@something.com"
                            value={this.state.userEmail}
                            onChange={(event)=>{
                                this.setState({
                                    userEmailError:"",
                                    userEmail:event.target.value
                                });
                            }}/>
                            {
                                this.state.userEmailError !== "" ? <div className="err">{this.state.userEmailError}</div> : null
                            }
                        <input 
                            className="custom-input big" 
                            type="text" 
                            placeholder="Your Name (e.g. John Doe)"
                            value={this.state.userName}
                            onChange={(event)=>{
                                this.setState({
                                    userNameError:"",
                                    userName:event.target.value
                                });
                            }}/>
                            {
                                this.state.userNameError !== "" ? <div className="err">{this.state.userNameError}</div> : null
                            }
                        <textarea
                            className="custom-input big"
                            placeholder="Your message (Optional)"
                            style={{resize:"none"}}
                            value={this.state.userMessage}
                            onChange={(event)=>{
                                this.setState({
                                    userMessage:event.target.value
                                })
                            }}/>
                        <button disabled={this.state.apiCallInProgress} className="send-btn" type="submit" onClick={(e)=>{
                            e.preventDefault();
                            e.stopPropagation();
                            this.sendEmail();
                        }}>
                            {
                                this.state.apiCallInProgress ? "PLEASE WAIT..." : "SEND"
                            }
                        </button>
                        {
                            this.state.successMessage !== "" ?
                            <div className="success-message">{this.state.successMessage}</div>
                            :
                            null
                        }
                    </form>
                </section>
            </div>
        );
    }
}