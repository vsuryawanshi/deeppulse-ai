import React, {Component} from "react";
import { browserHistory } from 'react-router';
import axios from "axios";


const EMAIL_REGEX = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

export default class Footer extends Component{
    constructor(props){
        super(props);
        this.state = {
            userEmail:"",
            userEmailError:"",
            userName:"",
            userNameError:"",
            userMessage:"",
            successMessage:"",
            showSuccessMessage:false
        }
    }

    sendEmail(){
        if(this.state.userEmail == ""){
            this.setState({
                userEmailError:"Please enter the email"
            });
        }
        if(this.state.userName == "") {
            this.setState({
                userNameError:"Please enter the username"
            });
        }
        if(!EMAIL_REGEX.test(this.state.userEmail)){
            this.setState({userEmailError:"Please enter a valid email"});
        }
        if(this.state.userEmail !== "" && this.state.userName !== ""){
            axios({
                method:"POST",
                url:"http://128.199.172.82:8090/user",
                data:{
                    name:this.state.userName,
                    email:this.state.userEmail,
                    message:this.state.userMessage
                }
            }).then((response)=>{
                this.setState({
                    userEmail:"",
                    userName:"",
                    userMessage:"",
                    showSuccessMessage:true
                },()=>{
                    setTimeout(()=>{
                        this.setState({
                            showSuccessMessage:false
                        });
                    },4000)
                })
            })
        }
    }

    render(){
        return(
            <div className="footer-section">
                <div className={`success-modal-wrap` + (this.state.showSuccessMessage ? " show" : "")}>
                    <div className="success-modal">
                        <div className="msg">
                            Thanks for your interest and getting in touch with us. Our team should be in touch with you shortly.
                        </div>
                        <div className="ok-btn" onClick={()=>{
                            this.setState({
                                showSuccessMessage:false
                            });
                        }}>OK</div>
                    </div>
                </div>
                <div className="width-wrap">
                    <section className="footer-comp">
                        <img src={require("../../images/deeppulse.svg")} className="footer-img"/>
                        <div className="footer-tagline">
                            AI powered sales assistant for your digital businesses
                        </div>
                        <div className="footer-social-links">
                            <img className="fsocial" onClick={()=>{
                                window.open("https://twitter.com/PulseDeep","_blank");
                            }} src={require("../../images/twitter-dark.svg")}/>
                            <img className="fsocial" onClick={()=>{
                                window.open("https://www.linkedin.com/company/deeppulse/","_blank");
                            }} src={require("../../images/linkedin-dark.svg")}/>
                        </div>
                    </section>
                    <section className="footer-links">
                        <div className="ftitle">COMPANY</div>
                        <div className="footer-item" onClick={()=>{
                            browserHistory.push("terms");
                            document.body.scrollTop = 0; // For Safari
                            document.documentElement.scrollTop = 0; // For Chrome, Firefox, IE and Opera
                        }}>Terms &amp; Conditions</div>
                        <div className="footer-item" onClick={()=>{
                            browserHistory.push("privacy");
                            document.body.scrollTop = 0; // For Safari
                            document.documentElement.scrollTop = 0; // For Chrome, Firefox, IE and Opera
                        }}>Privacy Policy</div>
                        <div className="footer-item" onClick={()=>{
                            browserHistory.push("cookie");
                            document.body.scrollTop = 0; // For Safari
                            document.documentElement.scrollTop = 0; // For Chrome, Firefox, IE and Opera
                        }}>Cookie Policy</div>
                    </section>
                    <section className="footer-form">
                        <div className="ftitle">GET IN TOUCH</div>
                        <input 
                            className="custom-input" 
                            type="email" 
                            placeholder="youremail@something.com"
                            value={this.state.userEmail}
                            onChange={(event)=>{
                                this.setState({
                                    userEmailError:"",
                                    userEmail:event.target.value
                                });
                            }}/>
                            {
                                this.state.userEmailError !== "" ? <div className="err">{this.state.userEmailError}</div> : null
                            }
                        <input 
                            className="custom-input" 
                            type="text" 
                            placeholder="Your Name (e.g. John Doe)"
                            value={this.state.userName}
                            onChange={(event)=>{
                                this.setState({
                                    userNameError:"",
                                    userName:event.target.value
                                });
                            }}/>
                            {
                                this.state.userNameError !== "" ? <div className="err">{this.state.userNameError}</div> : null
                            }
                        <textarea
                            className="custom-input"
                            placeholder="Your message (Optional)"
                            style={{resize:"none"}}
                            value={this.state.userMessage}
                            onChange={(event)=>{
                                this.setState({
                                    userMessage:event.target.value
                                })
                            }}/>
                        <div className="send-btn" onClick={()=>{
                            this.sendEmail();
                        }}>SEND</div>
                    </section>
                </div>
            </div>
        );
    }
}