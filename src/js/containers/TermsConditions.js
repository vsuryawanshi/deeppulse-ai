import React, { Component } from "react";
import Footer from "../common/FooterComp";

export default class TermsPage extends Component {
    windowLoadedCompletely(){
		var loaderElem = document.getElementById("loader-wrap");
		loaderElem.style.display = "none";
		setTimeout(()=>{
			//this.startImageAnimation();
		},200);
	}
	componentDidMount() {
		//this.loadImageOnCanvas(require("../../images/3.jpg"));       
		this.windowLoadedCompletely();
	}
    render() {
        return (
            <div className="terms-wrap">
                <div className="container">
                    <section className="heading-text">
                        <div className="title"><h1>Terms &amp; Conditions</h1></div>
                    </section>
                    <ul>
                        <li>Definitions and Interpretation<ul>
                                <li>The following are the standard terms and conditions under which DeepPulse Labs Private Limited (“the Company”) sells computer hardware, licences computer software and supplies related services. These Terms and Conditions shall, unless otherwise expressly stated in writing, apply to the subject matter of any agreement in respect thereof.</li>
                                <li>In these Terms and Conditions, unless the context otherwise requires, the following expressions have the following meanings:<br />>
                                <section className="withoutbulleat"><strong>“Acceptance Certificate”</strong>means a document to be used in conjunction with the supply of Products to be signed by the Customer on delivery indicating their acceptance of that delivery;<p></p>
                                    </section>
                                    <section className="withoutbulleat"><strong>“Agreement”</strong>means any agreements entered into between the Company and a Customer to which these standard Terms and Conditions apply;<p></p>
                                    </section>
                                    <section className="withoutbulleat"><strong>“Customer”</strong>means the individual, business, or other organisation with whom the Company contracts;<p></p>
                                    </section>
                                    <section className="withoutbulleat"><strong>“Supplier”</strong>means any supplier of Products or Services to the Company;<p></p>
                                    </section>
                                    <section className="withoutbulleat"><strong>“Products”</strong>means computer hardware, software and associated equipment that may be supplied by the Company; and<p></p>
                                    </section>
                                    <section className="withoutbulleat"><strong>“Services”</strong>means any service supplied by the Company.<p></p>
                                    </section>
                                </li>
                                <li>Any reference to a day or days refers to business days – that is any day which is not a weekend or public or bank holiday in the United Kingdom.</li>
                                <li>The headings in these Terms and Conditions are for convenience only and shall not affect their interpretation.</li>
                            </ul>
                        </li>
                        <li>Customer OrdersCustomer orders, if accepted by the Company, shall be subject to these Terms and Conditions and to the availability of all relevant Products and Services.</li>
                        <li>Price ListsPrice lists, catalogues and any other promotional material supplied by the Company do not constitute contractual offers capable of acceptance. Subject to sub-clause 4.c of these Terms and Conditions, prices shown in any such materials may be subject to change at any time prior to the entry by the Company and the Customer into a binding Agreement.</li>
                        <li>Quotations
<ul>
                                <li>All quotations are deemed to be subject to these Terms and Conditions and shall be valid for 30 days unless otherwise stated on the quotation.</li>
                                <li>The Company reserves the right to withdraw or amend any quotation prior to the Agreement.</li>
                                <li>The Company reserves the right to withdraw or amend any quotation following the Agreement where:
<ul>
                                        <li>Products or Services are withdrawn by the Supplier;</li>
                                        <li>the Supplier increases the charges for Products or Services to the Company; or</li>
                                        <li>specifications of Products or Services are varied by the Supplier.</li>
                                    </ul>
                                </li>
                            </ul>
                        </li>
                        <li>Product Specifications
<ul>
                                <li>The Company shall use reasonable endeavours to advise the Customer of variations to Product specifications following formal notification to the Company of such variations by the Supplier.</li>
                                <li>Where changes to Product specifications significantly alter the price or fitness for purpose of the Products the Company and the Customer shall agree upon such changes in writing or arrange for the supply of alternative Products.</li>
                                <li>Changes to Product specifications shall not provide grounds for cancellation of Customer orders unless such cancellation is agreed to in writing by the Company and the Customer.</li>
                            </ul>
                        </li>
                        <li>Hardware and Software Products
<ul>
                                <li>Products shall be supplied by the Company on the terms and conditions of use for such Products as defined by the Suppliers at the time of delivery.</li>
                                <li>The Company gives no warranty to the Customer in respect of Product that is purchased by the Company from a Supplier for resale to the Customer but shall take reasonable steps to assist Customer in pursuing warranty claims against the relevant Supplier.</li>
                                <li>Unless otherwise specified in the Agreement, the Company shall only deliver non-modifiable and executable run-time versions of Software.</li>
                                <li>The Customer must comply with the terms of the Supplier’s software licenses.</li>
                            </ul>
                        </li>
                        <li>Delivery and Acceptance
<ul>
                                <li>Unless it is agreed otherwise delivery shall be to the Customer’s address as specified in the Agreement.</li>
                                <li>The Company shall not be liable for any shortfalls in delivery or variation from Product specification on delivery unless a claim in writing is made by the Customer within 7 days of delivery.</li>
                                <li>In circumstances where the Company has attempted to physically deliver Products to the Customer and the Customer is unable or unwilling to accept such delivery, the Customer will be charged for the cost of the failed delivery in addition to any and all subsequent attempts. If the Customer is unable to accept delivery, a new date shall be set by mutual agreement of the parties. If the Customer is unwilling to accept delivery, the parties shall seek to vary the Agreement as appropriate by mutual agreement or the Customer shall seek to terminate the Agreement in accordance with Clause 18 of these Terms and Conditions.</li>
                                <li>Where the necessity for such has been agreed in advance and not otherwise the Customer shall sign the Company’s Acceptance Certificate stating on signature any defects or exclusions.</li>
                                <li>Acceptance of a delivery requiring an Acceptance Certificate is deemed to occur on the signing of the Certificate on the date of delivery, which date shall be recorded on the Certificate.</li>
                                <li>The Company shall on the signing of the Acceptance Certificate be entitled to invoice the Customer.</li>
                                <li>If, as a result of defects or exclusions in a delivery of Products or the provision of Services, the Customer does not sign a required Acceptance Certificate, further work may be agreed between the parties to remedy such defects. The Company shall use all reasonable endeavours to undertake such work without delay.</li>
                                <li>If, as a result of defects or exclusions in a delivery of Products or the provision of Services, the Customer does not sign a required Acceptance Certificate and subsequently uses the Hardware or Software or the results of Services provided without prior Agreement as to any remedial work on the part of the Company then the Customer is deemed to have accepted the same.</li>
                            </ul>
                        </li>
                        <li>Warranty
<ul>
                                <li>Subject to Clause 6.b of these Terms and Conditions and in respect of Product which is directly produced by the Company or Services provided directly by the Company, the only warranty given by the Company to the Customer is that the Company shall in accordance with normally accepted professional standards make good as quickly as is reasonably possible and at its own expense any defects identified on any relevant Acceptance Certificate or which develops during a period of 30 days after delivery of the Product or performance of the Services.</li>
                                <li>The Company does not warrant that the Products are free from minor errors not materially affecting performance. Such errors shall not be rectified in the absence of a prior written agreement to the contrary.</li>
                                <li>The undertaking given in this Clause shall not apply if the Product has been altered by any party other than the Company or has been operated or run on any platform or in any environment inappropriate for the Product.</li>
                            </ul>
                        </li>
                        <li>Return of Products
<ul>
                                <li>The return of Products shall be at the sole discretion of the Company but in any circumstance where the Company agrees to accept return of Products for any reason then the Customer shall:
<ul>
                                        <li>advise the Company within 3 days from the date of delivery of Products by the Company of the reason(s) for the return of Products;</li>
                                        <li>obtain a Returns Form from the Company prior to any return of Products;</li>
                                        <li>complete and return to the Company the Returns From to arrive at the Company within 7 days from the date of delivery of Products by the Company;</li>
                                        <li>properly pack the Products in the original packing where possible and include a detailed packing list;</li>
                                        <li>return the Products in the condition in which they were received to arrive at the Company within 14 days from the date of delivery of Products by the company; and</li>
                                        <li>take no action to effect any warranties that may cover the Products.</li>
                                    </ul>
                                </li>
                                <li>The Company shall be entitled to levy to the Customer a reasonable administration charge [amounting to no more than the cost of return delivery and the staff time spent on handling the return] in respect of return of Products and the Customer shall pay the same to the Company within 14 days of invoice.</li>
                            </ul>
                        </li>
                        <li>Title and Risk
<ul>
                                <li>Risk of loss or damage in respect of any tangible item shall pass to the Customer on delivery or collection of the item by the Customer or his agent.</li>
                                <li>The legal and beneficial ownership of Products and/or associated material supplied as part of Products and/or Services shall remain with the Company until payment in full in respect of all such Products and associated material supplied as part of Products and/or Services has been received by the Company in accordance with the terms of the Agreement.</li>
                                <li>Until such payment is received in full the Company may without prejudice to any of its rights recover or resell any of the Products and/or associated material and may enter upon the Customer’s premises by its servants or agents for that purpose.</li>
                                <li>Where a licence shall be granted by a Supplier and/or the Company to the Customer then the Customer shall not have the benefit of the licence until payment in full has been received by the Company.</li>
                            </ul>
                        </li>
                        <li>Charges
<ul>
                                <li>The Company shall render to the Customer an invoice or series of invoices in pounds sterling pursuant to the supply of Products and Services.</li>
                                <li>Charges specified in the Agreement [do not] include Value Added Tax which, if applicable, shall be added at the rate in force at the time of supply.</li>
                                <li>Unless specified in the Agreement and subject to Clause 7 of these Terms and Conditions:
<ul>
                                        <li>all Products shall be invoiced on the date of despatch to the Customer or collection of Products by the Customer or his agent; and</li>
                                        <li>all Services shall be invoiced in full and in advance.</li>
                                    </ul>
                                </li>
                                <li>Without prejudice to any other rights the Company may have in respect of any failure by the Customer to pay the charges or other monies payable pursuant to the Agreement, the Company may charge interest at the rate 3% above the base rate of the Bank of England from time to time in force, after as well as before judgement on any amount due from the Customer to the Company from the date due for payment until payment is received.</li>
                                <li>In the case of supply to a Customer outside the UK the Customer shall be responsible for all import levies, customs duties or other similar taxes of whatever nature.</li>
                                <li>Where travel and subsistence expenses are incurred by the Company, a 5% administration charge shall be added to these expenses and such expenses as surcharged shall be payable to the Company by the Customer within 14 days of being invoiced.</li>
                            </ul>
                        </li>
                        <li>Payment
<ul>
                                <li>The time stipulated for payment shall be of the essence of the Agreement and failure to pay within the period specified shall, in the absence of a written explanation from the Customer that has been duly accepted by the Company, render the Customer in material breach of the Agreement.</li>
                                <li>Invoices shall be payable in Pounds Sterling within any other period stated for a particular charge or invoice but in any event no later than 30 days of the invoice date.</li>
                                <li>If payment of any invoice is otherwise due it shall become automatically due immediately on the commencement of any act or proceeding in which the Customer’s solvency is involved.</li>
                            </ul>
                        </li>
                        <li>Customer’s Obligations
<ul>
                                <li>During the continuance of the Agreement the Customer shall:
<ul>
                                        <li>provide, free of charge, reasonable usage of machine time, communications, stationery, media, suitable working accommodation and access deemed necessary by the Company to fulfil the Agreement and shall provide an appropriate environment or platform to enable the Company to provide the Services or test run any Product and, in particular, the Customer warrants to the Company that the Customer shall provide and environment capable of receiving the Services or Products;</li>
                                        <li>furnish the Company promptly upon receipt of a request such information as the Company may reasonably require for the provision of the Services;</li>
                                        <li>nominate prior to the provision of any of the Services under the Agreement an authorised representative to be its prime point of contact with the Company during the continuance of the Agreement;</li>
                                        <li>ensure the accuracy and validity of all data and technical information provided to the Company;</li>
                                        <li>allow the Company reasonable access to its employees for the purpose of investigation and discussion in connection with the Agreement and ensure that its employees cooperate fully with the Company in relation to the provision of the Services;</li>
                                        <li>provide free and safe access to the Location as is necessary by the Company to comply with its obligations under the Agreement; and</li>
                                        <li>ensure that equipment provided by the Company for the purpose and provision of the Agreement shall not be modified, changed or removed without prior written permission of the Company. Where such equipment is modified, changed or removed then the cost of restoring or replacing the equipment shall be recovered from the Customer.</li>
                                    </ul>
                                </li>
                                <li>The Company and the Customer shall indemnify each other and keep each other fully and effectively indemnified against any loss of or damage to any property or injury to or death of any persons caused by negligent act or omission, wilful misconduct or breach of contract by the other, its employees or agents.</li>
                            </ul>
                        </li>
                        <li>Performance
<ul>
                                <li>The Company shall use its reasonable endeavours to comply with any day or dates for despatch or delivery of Products and for the supply of Services as stated in the Agreement. Unless the Agreement contains express provisions to the contrary, such dates shall constitute only statements of expectation and shall not be binding. If the Company, having used its reasonable endeavours fails to despatch or deliver the Products, or to supply or complete the Services by such date or dates whether or not binding, such failure shall not constitute a breach of the Agreement. The Customer shall not be entitled to treat the Agreement as thereby repudiated or to rescind it or any ancillary Agreement in whole or in part or claim compensation for such failure or for any consequential loss or damage resulting therefrom.</li>
                                <li>When expedited delivery is agreed to by the Company and the Customer and necessitates overtime or other additional costs, the Customer shall reimburse the Company for the amount of such overtime payment or other costs and shall pay the same within 30 days of invoice.</li>
                                <li>If performance of the Agreement is suspended at the request of or delayed through default of the Customer including, but without prejudice to the generality of the foregoing, incomplete or incorrect instructions, or refusal to accept delivery of the Products or Services for a period of 30 days, the Company shall be entitled to payment at the then prevailing rates for the Services already performed, Products supplied or ordered and any other additional costs thereby incurred and the Customer shall pay such sums within 30 days of invoice.</li>
                            </ul>
                        </li>
                        <li>Business Associates and Delegation
<ul>
                                <li>The Company may delegate any of its obligations or responsibilities arising out of the Agreement to any of its business associates. Performance by such associates shall be deemed to be performance by the Company.</li>
                                <li>The Customer may not assign the benefit or burden of the Agreement in any way.</li>
                                <li>At the written request of the Customer the Company may, at its sole discretion, agree to novation of the Agreement. Such agreement must be evidenced in writing.</li>
                            </ul>
                        </li>
                        <li>Proprietary Rights
<ul>
                                <li>Unless otherwise specified in the Agreement, copyright and all other proprietary rights in the Products and associated documentation and any documentation supplied in respect of the Services and all parts and copies thereof shall remain vested in the Company or, for third party Products, in the Supplier.</li>
                                <li>In respect of software where the proprietary rights are vested in the Company only a non-exclusive, non-transferable licence for the purpose for which the software has been made available to the Customer is deemed to be granted by the Company and only then on condition that the Customer fulfils all of their relevant obligations arising out of the Agreement.</li>
                            </ul>
                        </li>
                        <li>Liability
<ul>
                                <li>The following provisions set out the Company’s entire liability (including any liability for the acts and omissions of its employees) to the Customer in respect of:
<ul>
                                        <li>any breach of its contractual obligations arising out of the Agreement; and</li>
                                        <li>any representation, statement or delictual or tortious act or omission, including negligence arising out of or in connection with the Agreement.</li>
                                    </ul>
                                </li>
                                <li>The Customer’s attention is drawn to the following provisions:
<ul>
                                        <li>the Company’s liability to the Customer for death or injury resulting from its own or that of its employee’s negligence shall not be limited;</li>
                                        <li>any act or omission on the part of the Company falling within this clause shall known as an “Event of Default”; and</li>
                                        <li>subject to the limit set out below the Company shall accept liability to the Customer in respect of damage to the tangible property of the Customer resulting from the negligence of the Company or its employees or the breach of contract by the Company.</li>
                                    </ul>
                                </li>
                                <li>Subject to the provisions of clause 17.b.i:
<ul>
                                        <li>the Company’s entire liability in respect of any Event of Default shall be limited to the value of the Agreement;</li>
                                        <li>the Company shall not be liable to the Customer in respect of any Event of Default for loss of profits goodwill or any type of special indirect or consequential loss (including loss or damage suffered by the Customer as a result of an action brought by a third party) even if such loss was reasonably foreseeable or the Company had been advised of the possibility of the Customer incurring the same. If a number of Events of Default give rise substantially to the same loss then they shall be regarded as giving rise to only one claim under this Agreement; and</li>
                                        <li>the Company shall have no liability to the Customer in respect of any Event of Default unless the Customer shall have served notice of the same upon the Company within one year of the date it became aware of the circumstances giving rise to the Event of Default or the date when it ought reasonably to have become so aware.</li>
                                    </ul>
                                </li>
                                <li>The Customer hereby agrees to afford the Company not less than 30 days in which to remedy any Event of Default.</li>
                                <li>Nothing in this clause shall confer any right or remedy upon the Customer to which it would not otherwise be legally entitled.</li>
                            </ul>
                        </li>
                        <li>Cancellation of OrderThe Customer shall not be entitled to cancel any order for Product(s) and/or Service(s) or any part thereof except upon terms which reimburse the Company for loss of Profit and all costs, charges and expenses incurred by the Company in respect of the Product(s) and/or Service(s) or any part thereof up to the date of receipt by the Company of written notification of cancellation form the Customer.</li>
                        <li>Termination
<ul>
                                <li>Without prejudice to any other provision contained within these Terms and Conditions or of any Agreement the Company may terminate the Agreement by notice in writing in any of the following events:
<ul>
                                        <li>the Customer commits a material breach of the Agreement which is incapable of remedy; or</li>
                                        <li>the Customer commits a material breach which is capable of remedy but which the Customer fails to remedy within 14 days of written notice by the Company specifying the event of default and requiring its remedy.</li>
                                    </ul>
                                </li>
                                <li>The Company and the Customer may by notice in writing to the other terminate the Agreement if the other shall have a receiver or liquidator appointed, shall pass a resolution for winding up (otherwise than for the purpose of amalgamation or reconstruction), if a Court shall make an order to that effect, if the other party shall enter into composition or arrangement with its creditor(s) or shall become insolvent. Such an event shall be deemed to be a material breach incapable of remedy.</li>
                            </ul>
                        </li>
                        <li>Consequences of Termination
<ul>
                                <li>Any termination of the Agreement howsoever caused shall not affect any accrued rights or liabilities of either the Company or the Customer arising out of the Agreement;</li>
                                <li>On termination of the Agreement for any reason, the Customer shall return forthwith to the Company the Products and all copies thereof, the documentation and the media supplied therewith and other items in the possession of the Customer which are the property of the Company.</li>
                            </ul>
                        </li>
                        <li>Intellectual Property Indemnity
<ul>
                                <li>The Company shall indemnify and hold the Customer and its employees from and against all loss and damage and cost and expense resulting from or arising out of any threatened or actual infringement of patents, copyright, registered designs or other intellectual property rights belonging to any party provided that the Customer shall:
<ul>
                                        <li>notify the Company in writing of any allegation or infringement;</li>
                                        <li>make no admission without the Company’s consent; and</li>
                                        <li>at the Company’s request allow the Company to conduct and/or settle all negotiations in or prior to litigation and give the Company all reasonable assistance in respect thereof.</li>
                                    </ul>
                                </li>
                            </ul>
                        </li>
                        <li>Confidentiality
<ul>
                                <li>The Company and the Customer shall keep confidential the following:
<ul>
                                        <li>the Agreement and all other information of the other party obtained under, or in connection with, the Agreement; and</li>
                                        <li>all oral communications, representations and information of any kind made by either party or their representatives or advisors pursuant to the conclusion or fulfilment of the Agreement.</li>
                                    </ul>
                                </li>
                                <li>The provisions of this Clause 22 shall not apply to:
<ul>
                                        <li>any disclosure of the information contained in Clauses 22.a.i and 22.a.ii for which the written agreement of both parties has been obtained;</li>
                                        <li>any information in the public domain otherwise than as a result of a breach of the Agreement;</li>
                                        <li>information that was already in the possession of the receiving party prior to disclosure by the other party; and</li>
                                        <li>information obtained from a third party who is free to divulge the same.</li>
                                    </ul>
                                </li>
                                <li>The Company and the Customer shall divulge confidential information only to those employees who are directly involved in the use of the Product(s) and shall ensure that such employees are aware of and comply with these obligations as to confidentiality.</li>
                                <li>The obligations of the parties as to disclosure and confidentiality shall come into effect on the signing of the Agreement and shall continue in force notwithstanding the termination of the Agreement.</li>
                            </ul>
                        </li>
                        <li>Health and Safety
<ul>
                                <li>The Customer shall take all reasonable precautions to ensure the health and safety of the Company’s employees while on the Customer’s premises.</li>
                                <li>The Company shall not be liable to the Customer in any civil proceeding brought by the Customer against the Company under any Health and Safety Regulations, except where such exclusion of liability is prohibited by law.</li>
                                <li>The Customer shall indemnify and keep indemnified the Company in respect of any liability, monetary penalty or fine in respect of or in connection with the Product(s) and Service(s) incurred directly or indirectly by the Company under any Regulations, orders or directions made thereunder arising or resulting from the Customer’s default.</li>
                            </ul>
                        </li>
                        <li>Notices
<ul>
                                <li>Any notice pursuant to the Agreement shall be in writing signed by a Director of the Company or by some person duly authorised by a Director of the Company and shall be delivered personally, sent by prepaid recorded delivery (airmail if overseas) or by facsimile transmission to the party due to receive such notice at the address of the party as shown in the Agreement or to such other address as shall be notified in writing to the other party to the Agreement form time to time.</li>
                                <li>Any notice delivered personally shall be deemed to be received when delivered. Any notice sent by prepaid recorded delivery shall be deemed (in the absence of evidence of earlier receipt) to be received 48 hours after posting (6 days if sent by airmail). In proving the time of despatch it shall be sufficient to show that the envelope containing such notice was properly posted.</li>
                                <li>Any notice sent by facsimile transmission shall be deemed to have been received upon receipt by the sender of the correct transmission report.</li>
                            </ul>
                        </li>
                        <li>ArbitrationSubject to the agreement of the parties, if any dispute or difference shall arise between the Company and the Customer on any matter relating to or arising out of the Agreement, such a dispute shall be referred to the arbitration of a single Arbitrator to be agreed upon by the parties or failing agreement to be appointed by the then President of the Law Society of England and Wales.</li>
                        <li>WaiverThe rights and remedies of either party under the Agreement shall not be diminished, waived or extinguished by the granting of any indulgence, forbearance or extension of time by the other party nor any failure or delay by the other party in asserting or exercising any such rights or remedies.</li>
                        <li>SeveranceIf at any time any one or more clause, sub-clause, paragraph, subparagraph or any other part of the Agreement or these Terms and Conditions is held to be, or becomes, void or otherwise unenforceable for any reason under any applicable law the same shall be deemed omitted and the validity and/or enforceability of the remaining provisions of the Agreement or these Terms and Conditions shall not any way be affected or impaired thereby.</li>
                        <li>VariationNo variation in the provisions of the Agreement shall be of any effect unless made in writing and signed on behalf of the Customer and the Company.</li>
                        <li>Set-OffNeither the Company nor the Customer is entitled to set-off any sums in any manner from payments due or sums received in respect of any claim under the Agreement or any other agreement at any time.</li>
                        <li>Force Majeure
<ul>
                                <li>In the event that either party is prevented from fulfilling its obligations under the Agreement by reason of any supervening event beyond its control including but not limited to war, national emergency, flood, earthquake, strike or lockout (subject to Sub-clause 30.b) the party shall not be deemed to be in breach of its obligations under the Agreement. The party shall immediately give notice of this to the other party and must take all reasonable steps to resume performance of its obligations.</li>
                                <li>Sub-clause 30.a shall not apply with respect to strikes and lockouts where such action has been induced by the party so incapacitated.</li>
                                <li>Each party shall be liable to pay to the other damages for any breach of this Agreement and all expenses and costs incurred by that party in enforcing its rights under this Agreement.</li>
                                <li>If and when the period of such incapacity exceeds 6 months then this Agreement shall automatically terminate unless the parties first agree otherwise in writing.</li>
                            </ul>
                        </li>
                        <li>Non SolicitationNeither the Customer nor the Company shall during the term of the Agreement and for a period of 6 months thereafter solicit or entice away or endeavour to solicit or entice away from the other any employee who has worked under the Agreement without written consent of the other.</li>
                        <li>Law and JurisdictionThe Agreement shall be governed by and construed in accordance with the laws of England and Wales. Any dispute concerning it or its interpretation shall be adjudicated in that Jurisdiction.</li>
                    </ul>



                </div>
                <Footer />
            </div>
        )
    }
}